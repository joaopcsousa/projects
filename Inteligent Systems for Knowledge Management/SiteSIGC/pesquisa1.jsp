<h3>Genres</h3>



<table class="table table-striped">
    <thead>
      <tr>
      <td><b>Name</b></td>
      <td><b>Movies</b></td>

      </tr>
    </thead>
    <tbody>

<% 

  for(int i=0;listaGeneros!=null && i<listaGeneros.size();i++){
%>

    <tr>
      <td><a href="genre.jsp?p=<%=listaGeneros.get(i).getId()%>&name=<%=listaGeneros.get(i).getName()%>"><%=listaGeneros.get(i).getName()%></a></td>
      <td>
<%
  listaAux=bean.getMoviesGenreURI(listaGeneros.get(i).getName());
        for(int a=0;a<listaAux.size();a++)
          out.println(listaAux.get(a)+",");
%> 
    </td>
     
    </tr>
<%
  }
%>
  </tbody>
  </table>








<h3>Crew</h3>



<table class="table table-striped">
    <thead>
      <tr>
      <td><b>Name</b></td>
      <td><b>Jobs</b></td>

      </tr>
    </thead>
    <tbody>

<% 

  for(int i=0;listaCrew!=null && i<listaCrew.size();i++){
%>

    <tr>
      <td><a href="individual.jsp?p=<%=listaCrew.get(i).getId()%>&name=<%=listaCast.get(i).getName()%>"><%=listaCrew.get(i).getName()%></a></td>
      <td>
<%
  listaAux=bean.getJobCrewURI(listaCrew.get(i).getName());
        for(int a=0;a<listaAux.size();a++)
          out.println(listaAux.get(a)+",");
%> 
    </td>
     
    </tr>
<%
  }
%>
  </tbody>
  </table>



<h3>Cast</h3>



<table class="table table-striped">
    <thead>
      <tr>
      <td><b>Name</b></td>
      <td><b>Characters</b></td>

      </tr>
    </thead>
    <tbody>

<% 

  for(int i=0;listaCast!=null && i<listaCast.size();i++){
%>

    <tr>
      <td><a href="individual.jsp?p=<%=listaCast.get(i).getId()%>&name=<%=listaCast.get(i).getName()%>"><%=listaCast.get(i).getName()%></a></td>
      <td>
<%
  listaAux=bean.getCharacterCastURI(listaCast.get(i).getName());
        for(int a=0;a<listaAux.size();a++)
          out.println(listaAux.get(a)+",");
%> 
    </td>
     
    </tr>
<%
  }
%>
  </tbody>
  </table>