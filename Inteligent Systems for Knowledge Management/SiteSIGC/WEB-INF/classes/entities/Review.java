package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Review{

	private int id;
	private String author;
	private String comment;
	private double rank;

	public Review(){
		
	}

	public String getAuthor() {
		return author;
	}


	public String getComment(){
		return comment;
	}
	

	public void setAuthor(String author) {
		this.author=author;
	}


	public void setComment(String comment){
		this.comment=comment;
	}
	
	public int getId() {
		return id;
	}

	public void setRank(double rank){
		this.rank=rank;
	}
	
	public double getRank() {
		return rank;
	}
}
