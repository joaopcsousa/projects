package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class Actor implements Serializable{



	private int id;
 
	People people;
	 
	Character character;
	
	public Actor(){
		
	}
	
	public Actor(People people,Character character){
		this.people=people;
		this.character=character;
	}
	
	public Character getCharacter(){
		return character;
	}
	
	public People getPeople(){
		return people;
	}
}
