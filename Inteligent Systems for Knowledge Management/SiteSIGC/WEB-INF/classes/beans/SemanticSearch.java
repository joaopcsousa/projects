package beans;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.tdb.TDBFactory;

import entities.Genre;
import entities.Movie;
import opennlp.tools.namefind.RegexNameFinder;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;


public class SemanticSearch {

	
	public SemanticSearch(){

	}
	
	public ArrayList<ResultFinal> searcher(String query){
		InputStream is=null;
		Tokenizer tokenizer=null;
		try {
			is = new FileInputStream("/Users/josericardoramos/Documents/workspace/ProjetoWS/libs/en-token.bin");
	
			TokenizerModel model = new TokenizerModel(is);
	 
			tokenizer = new TokenizerME(model);
			is.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String tokens[] = tokenizer.tokenize(query);

		for(int i=0;i<tokens.length;i++){
			tokens[i]=tokens[i].toLowerCase();
		}

		ArrayList<String> classes=getClasses(tokens);
		ArrayList<String> jobs=getJobs(tokens);
		ArrayList<String> languages=getLanguages(tokens);
		ArrayList<String> rank=getRank(tokens);
		ArrayList<String> date=getDate(tokens);
		ArrayList<String> genres=getGenres(tokens);
		removeStopWords(tokens);
		ArrayList<String> other=getInteresting(tokens);
		//ArrayList<String> other=getOther(tokens);
		
		return (new Query()).semanticSearch(classes,jobs,languages,rank,date,genres,other);
		//makeQuery(classes,jobs,languages,rank,date,genres,other);
	}
	
	public void removeStopWords(String[] tokens){
		for(int i=1;i<tokens.length-1;i++){
			if(tokens[i].matches("(or|and|of|the)") && tokens[i-1].compareTo("!")==0 && tokens[i+1].compareTo("!")==0){
				tokens[i]="?";
				//System.out.println(tokens[i]);
			}
		}
	}
	
	
	public ArrayList<String>  getClasses(String[] tokens){
		String classes="(((f|F)ilm)|((M|m)ovie)|((C|c)haracter)|((c|C)ompan(y|ies))|((A|a)ctor)|((G|g)enre)|((j|J)ob)|((C|c)ountry)|((L|l)anguage)|((R|r)ank)|((Y|y)ear)|((D|d)ate))(s)?";
		int prevEnd=0;
		Pattern p = Pattern.compile(classes);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		int check=0;
		ArrayList<String> result=new ArrayList<String>();
		
		for(Span s: nameSpans){
			check++;
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println("Class "+token);
			

		}
		return result;
		
	}
	
	public ArrayList<String> getJobs(String[] tokens){
		String jobs="(((d|D)irector)|((p|P)roducer)|((o|O)riginal (m|M)usic (c|C)omposer)|((d|D)irector of (p|P)hotography)|((p|P)roduction (d|D)esign)|((c|C)asting)|((e|E)ditor)|((s|S)creenplay)|(Art Direction)|(Author)|(Executive Producer)|(Writer)|((M|m)usic)|((p|P)roduction Manager)|((N|n)ovel)|(Animation)|((S|s)pecial Effects)|((S|s)tory)|((M|m)usical)|((p|P)roduction Supervisor)|((O|o)riginal Story)|((c|C)reator))(s)?";

		//String numbers="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		
		
		Pattern p = Pattern.compile(jobs);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		int check=0;
		ArrayList<String> result=new ArrayList<String>();
		
		for(Span s: nameSpans){
			check++;
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println("Jobs "+token);
		}
		
		return result;
	}
	
	public ArrayList<String> getGenres(String[] tokens){
		String genres="(((a|A)ction)|((c|C)omedy)|((c|C)rime)|((t|T)hriller)|((d|D)rama)|((d|D)ocumentary)|((r|R)omance)|(Foreign)|((a|A)dventure)|((s|S)cience Fiction)|(Family)|((h|H)orror)|((m|M)usic)|((m|M)ystery)|((h|H)istory)|(Fantasy)|((a|A)nimation)|((s|S)hort)|((m|M)usical)|(War)|(Indie)|((r|R)oad Movie)|((s|S)uspense)|((e|E)astern)|((s|S)ports Film)|(Western)|((d|D)isaster)|(Film Noir)|((h|H)oliday)|((s|S)port)|((e|E)rotic))";

		//String numbers="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		
		
		Pattern p = Pattern.compile(genres);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		int check=0;
		ArrayList<String> result=new ArrayList<String>();
		
		for(Span s: nameSpans){
			check++;
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println("Genres "+token);
		}
		
		return result;
	}
	
	public ArrayList<String> getDate(String[] tokens){
		String date="((1|2)(9|0)(1|2|3|4|5|6|7|8|9|0)(1|2|3|4|5|6|7|8|9|0))";
		//String numbers="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		
		
		Pattern p = Pattern.compile(date);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		int check=0;
		ArrayList<String> result=new ArrayList<String>();
		
		for(Span s: nameSpans){
			check++;
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println("date :"+token);
		}
		
		return result;
	}
	
	public ArrayList<String> getRank(String[] tokens){
		String rank="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		//String numbers="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		
		
		Pattern p = Pattern.compile(rank);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		ArrayList<String> result=new ArrayList<String>();
		int check=0;
		
		
		for(Span s: nameSpans){
			check++;
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println("rank "+token);
		}
		return result;
		
	}
	
	public ArrayList<String> getOther(String[] tokens){
		String entity="(([A-Z]|[a-z])([a-z]|\\.)*)(( |-)(([A-Z]|&|[a-z]|\\.)*))*";
		//String numbers="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		
		Pattern p = Pattern.compile(entity);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		ArrayList<String> result=new ArrayList<String>();
		
		
		for(Span s: nameSpans){
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println("outros "+token);
		}
		
		return result;
	}

	public ArrayList<String> getInteresting(String[] tokens){
		int i=0;
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		Dataset dataset;
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
    	dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;

		String nameSpace="http://www.owl-ontologies.com/creationOwl#";

		String name="";
		String namePrev="";
		int campoAux=0;
		ArrayList<String> res=new ArrayList<String>();

		while(i<tokens.length){

			if(name.compareTo("")==0){

				name=tokens[i];
			}else{
				name+=" "+tokens[i];
			}

			System.out.println("Testar Novo!!->"+name+" "+tokens.length);
			if(!name.matches("([A-Z]|[a-z])([A-Z]|[a-z]|[0-9]| )*")){
				System.out.println("foste ");
				name=namePrev="";
				i++;
				continue;
			}
				
	   		String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT (COUNT(?name) as ?counter) WHERE {?x nameSpace:hasName ?name. ?x nameSpace:hasID ?id. FILTER (regex(?name,'(^|(.)* )"+name+"($| (.)*)','i')) "
		 		+"} GROUP BY ?name ";

			QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		  	ResultSet rs = qExec.execSelect() ;
		 	while(rs.hasNext()){
				QuerySolution row= rs.next();
				Iterator columns=row.varNames();

				while(columns.hasNext()){
					RDFNode cell = row.get(columns.next().toString());
					campoAux=Integer.parseInt(cell.asLiteral().toString().substring(0,cell.asLiteral().toString().indexOf("^")));
	    		}
	    	}
    	    System.out.println("Counter: "+campoAux);
			if(campoAux>0 && i+1!=tokens.length && tokens[i+1].matches("([A-Z]|[a-z])([A-Z]|[a-z]|[0-9])*")){
				namePrev=name;
			}else if(campoAux==0 && namePrev.compareTo("")!=0){
				System.out.println("A adincionar1...");
				res.add(namePrev);
				name="";
				namePrev="";
				continue;
			}else if((i+1==tokens.length && campoAux>0) ||  (i+1!=tokens.length && !tokens[i+1].matches("([A-Z]|[a-z])([A-Z]|[a-z]|[0-9])*"))){
				System.out.println("A adincionar2..."+name);
				res.add(name);
				name="";
				namePrev="";
			}else{
				name="";
				namePrev="";
			}
			

			i++;

		} 

		return res;

	}
	
	public ArrayList<String> getLanguages(String[] tokens){
		String languages="(((E|e)nglish)|((D|d)ansk)|((E|e)spanol)|((D|d)eutsch)|((F|f)rancais)|((P|p)olski)|((M|m)agyar)|((P|p)ortugues)|((I|i)taliano)|(Latin)|(Nederlands)|((S|s)uomi)|((S|s)venska)|((S|s)rpski)|((S|s)hqip)|((E|e)uskera)|(Kiswahili)|((S|s)omali)|((A|a)frikaans)|((I|i)siZulu)|(No Language)|(Gaeilge)|(Hrvatski))";
		//String numbers="((1|2|3|4|5|6|7|8|9|0)(.(1|2|3|4|5|6|7|8|9|0))*)";
		
		
		Pattern p = Pattern.compile(languages);
		Pattern[] listP={p};
		int i,end,init;
		RegexNameFinder rnf=new RegexNameFinder(listP);
		Span nameSpans[] = rnf.find(tokens);
		int check=0;
		ArrayList<String> result=new ArrayList<String>();
		
		
		for(Span s: nameSpans){
			check++;
			String token="";

			end=Integer.parseInt(s.toString().substring(s.toString().indexOf('.')+2,s.toString().length()-1));
			init=Integer.parseInt(s.toString().substring(1,s.toString().indexOf('.')));
			
			for(i=init;i<end;i++){
				token+=tokens[i];
				tokens[i]="!";
				if(end!=init+1 && i!=end-1)
					token+=" ";
			}
			result.add(token);
			System.out.println(token);
		}
		
		return result;
	}
}
