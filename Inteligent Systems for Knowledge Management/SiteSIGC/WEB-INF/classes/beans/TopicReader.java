package beans;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.*;
import entities.*;

public class TopicReader{
	public String folder="/usr/local/apache-tomcat-7.0.32/webapps/SiteSIGC/words_by_topic/";
    public ArrayList<TopicWords> topics;

   public TopicReader(String genreName){
   		topics=new ArrayList<TopicWords>();
   		readWords(genreName);
   }

   public void readWords(String name){
   		try {
			BufferedReader reader = new BufferedReader(new FileReader(folder+name+".txt"));
			String line = null;

			while ((line = reader.readLine()) != null) {
			    TopicWords topic=new TopicWords();
			    String[] words=line.split(",");

			    for(int i=0;i<words.length;i++){
			    	topic.words.add(words[i]);
			    }

				topics.add(topic);		    
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   }
    
}