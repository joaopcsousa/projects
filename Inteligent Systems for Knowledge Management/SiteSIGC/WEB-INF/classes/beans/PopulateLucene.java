package beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import opennlp.tools.namefind.RegexNameFinder;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import entities.Movie;

public class PopulateLucene {
	
	private IndexWriter indexWriter = null;
	private IndexSearcher searcher; 
	private QueryParser titleQueryParser; 
	private QueryParser contentQueryParser; 

    
	public PopulateLucene(){
		
		//writer();
		//searchPeople("person:'Brad Pitt' AND person:'Angelina Jolie");
		
	}
	
	
	public List<String> searchPeople(String name){
		List<String> res=new ArrayList<String>();
		try {
			searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(new File("/Users/josericardoramos/Documents/workspace/ProjetoWS/LuceneIndexFile")))); 
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36); // defining the query parser to search items by title field. 
			titleQueryParser = new QueryParser(Version.LUCENE_36, "person", analyzer); // defining the query parser to search items by content field. 
			
			Query query;
			query = titleQueryParser.parse(name);

			ScoreDoc[] queryResults = searcher.search((org.apache.lucene.search.Query) query, 10).scoreDocs; 

			for (ScoreDoc scoreDoc : queryResults) { 
				Document doc = searcher.doc(scoreDoc.doc); 
				res.add(doc.get("id"));
				System.out.println("Search: "+doc.get("title")+" "+doc.get("id"));
			} 
		}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		return res;
	}
	
	public List<String> searchMovies(String name){
		List<String> res=new ArrayList<String>();
		try {
			searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(new File("/Users/josericardoramos/Documents/workspace/ProjetoWS/LuceneIndexFile")))); 
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36); // defining the query parser to search items by title field. 
			titleQueryParser = new QueryParser(Version.LUCENE_36, "title", analyzer); // defining the query parser to search items by content field. 
			
			Query query;
			query = titleQueryParser.parse(name);

			ScoreDoc[] queryResults = searcher.search((org.apache.lucene.search.Query) query, 10).scoreDocs; 

			for (ScoreDoc scoreDoc : queryResults) { 
				Document doc = searcher.doc(scoreDoc.doc); 
				res.add(doc.get("id"));
				System.out.println("Search:"+doc.get("title")+" "+doc.get("id"));
			} 
		}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		return res;
	}

}
