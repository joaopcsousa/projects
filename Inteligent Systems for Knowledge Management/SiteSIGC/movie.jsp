<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:.</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <style type="text/css">
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
      .box-content {
          display: inline-block;
          width: 250px;
      }

      .border-row {
          border-bottom: 1px solid #ccc;
      }

      .left {
          border-right: 1px solid #ccc;
      }
    </style>
  </head>

<body>

   <jsp:include page="header.jsp"></jsp:include>
   <%@page import="beans.*,entities.*,java.util.ArrayList"%>
<%
    DBBean bean=new DBBean();
    ArrayList<Tuple> aux=new ArrayList<Tuple>();
    ArrayList<Review> reviews=new ArrayList<Review>();

    if(request.getParameter("p")!=null){
%>
<div class="span10">
<b>Movie Name:<%=request.getParameter("name")%> </b>
<br><br>
<b>Genre : </b><br>
<% 
  aux=bean.getGenresByMovieURI(Integer.parseInt(request.getParameter("p")));

  for(int i=0;i<aux.size();i++)
    out.println("<a href='genre.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a><br>");
%>
<br>
<div class="border-row">
<br>
</div>
<h4>Crew</h4>
<br>
<br>
<table class="table table-striped">  
        <thead>  
          <tr>  
            <th>Name</th>  
            <th>Job</th>  
          </tr>  
        </thead>  
        <tbody>  
<% 
  aux=bean.getCrewByMovieURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<tr><td><a href='individual.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a></td><td><a href='job.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a></td></tr>");
%>
</tbody>  
      </table>  
<br>
<div class="border-row">
<br>
</div>
<h4>Cast</h4>
<br>
<table class="table table-striped">  
        <thead>  
          <tr>  
            <th>Name</th>  
            <th>Character</th>  
          </tr>  
        </thead>  
        <tbody>  
<% 
  aux=bean.getCastByMovieURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<tr><td><a href='individual.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a></td><td><a href='character.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a></td></tr>");
%>
</tbody>  
      </table>  
</body>  
</html>  
<div class="border-row">
<br>
</div>
<% 
  reviews=bean.getMovieComments(Integer.parseInt(request.getParameter("p")));
%>
<h4>Reviews</h4>
<%
int i=0;
for(Review review:reviews){
  if(i%2==1){

    switch(((int)review.getRank())){
      case 1:
      case 2:
        out.print("<div class=\"alert alert-danger pull-right\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
      break;
      case 3:
        out.print("<div class=\"alert alert-warning pull-right\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
      break;
      case 4:
      case 5:
        out.print("<div class=\"alert alert-success pull-right\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
      break;
    }
    //out.print("<div class=\"alert alert-info pull-right\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");

  }else{
  if(i!=0)
    out.print("</br></br></br></br></br></br>");
    
    switch(((int)review.getRank())){
      case 1:
      case 2:
        out.print("<div class=\"alert alert-danger\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
      break;
      case 3:
        out.print("<div class=\"alert alert-warning\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
      break;
      case 4:
      case 5:
        out.print("<div class=\"alert alert-success\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
      break;
    }
  //out.print("<div class=\"alert alert-success\" style=\"width: 60%;\"><h4>"+review.getAuthor()+"</h4>"+review.getComment()+"</div>");
  }
  i++;
}


%>

<br>
<b>Related :</b>
<br> 
<% 
  bean=new DBBean();
  aux=bean.getRecomendation(Integer.parseInt(request.getParameter("p")));
  for(i=0;i<aux.size();i++){
    out.println("<a href='movie.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a>("+aux.get(i).description+")"+"<br>");
  }
}
%>
</div>
</body>
</html>