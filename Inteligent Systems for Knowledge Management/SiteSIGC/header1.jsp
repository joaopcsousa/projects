<%@page import="beans.*,entities.*,java.util.ArrayList,java.util.Map"%>
<html>
<head>

  <script type="text/javascript" src="js/jquery-1.4.2.min.js" ></script>
  <script src="js/jquery.autocomplete.js"></script> 

</head>
<body>


<div class="hero-unit ">
	<h1 style="text-align:center">.:Movie Website:.</h1>
</div>
<div class="navbar navbar-inverse">
  <div class="navbar-inner">
    <ul class="nav">
      <li class="active"><a href="index.jsp">Home Page</a></li>
      <li class="divider-vertical"></li>
    </ul>
    <ul class="nav">
      <li class="active"><a href="data_visualization.jsp">Data Visualization</a></li>
      <li class="divider-vertical"></li>
    </ul>
    	<form class="navbar-search pull-right" action="pesquisa.jsp" method="GET">
				<input name="semantica" id="semantica" class="input-mysize" type="text" class="input_text" placeholder="Search">
		  </form>

  </div>
</div>

 <div class="span2">
    <ul class="nav nav-pills nav-stacked">
      <li class="active"><a href="data_visualization.jsp">General View</a></li>
        <li><a href="data_visualization.jsp?name=Action">Action</a></li>
        <li><a href="data_visualization.jsp?name=Adventure">Adventure</a></li>
        <li><a href="data_visualization.jsp?name=Animation">Animation</a></li>
        <li><a href="data_visualization.jsp?name=Comedy">Comedy</a></li>
        <li><a href="data_visualization.jsp?name=Crime">Crime</a></li>
        <li><a href="data_visualization.jsp?name=Documentary">Documentary</a></li>
        <li><a href="data_visualization.jsp?name=Drama">Drama</a></li>
        <li><a href="data_visualization.jsp?name=Family">Family</a></li>
        <li><a href="data_visualization.jsp?name=Fantasy">Fantasy</a></li>
        <li><a href="data_visualization.jsp?name=Foreign">Foreign</a></li>
        <li><a href="data_visualization.jsp?name=History">History</a></li>
        <li><a href="data_visualization.jsp?name=Horror">Horror</a></li>
        <li><a href="data_visualization.jsp?name=Indie">Indie</a></li>
        <li><a href="data_visualization.jsp?name=Music">Music</a></li>
        <li><a href="data_visualization.jsp?name=Mystery">Mystery</a></li>
        <li><a href="data_visualization.jsp?name=Romance">Romance</a></li>
        <li><a href="data_visualization.jsp?name=Science_Fiction">Science Fiction</a></li>
        <li><a href="data_visualization.jsp?name=Suspense">Suspense</a></li>
        <li><a href="data_visualization.jsp?name=Thriller">Thriller</a></li>
        <li><a href="data_visualization.jsp?name=War">War</a></li>
        <li><a href="data_visualization.jsp?name=Western">Western</a></li>
    </ul>
 </div>
</body>
</html>