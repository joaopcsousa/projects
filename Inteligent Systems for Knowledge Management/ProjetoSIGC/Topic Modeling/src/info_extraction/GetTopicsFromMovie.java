package info_extraction;

import info_extraction.entities.Movie;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Pattern;

import cc.mallet.classify.Classifier;
import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.StringArrayIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;
import cc.mallet.pipe.iterator.*;

public class GetTopicsFromMovie {
	
	String[] genres;
	Movie movie;
	ArrayList<ParallelTopicModel> models;
	String topicWords;
	double max=0.0;
	InstanceList instances;
	
	public GetTopicsFromMovie(Movie movie){
		genres=new String[]{"Action","Adventure","Animation","Comedy","Crime","Drama","Family","Fantasy","Foreign","History","Horror","Indie","Music","Mystery","Romance","Science_Fiction","Suspense","Thriller","War"};
		this.movie=movie;
		
		System.out.println(movie.getDescription());
		//////
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
		pipeList.add( new CharSequenceLowercase() );
        pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
        pipeList.add( new TokenSequenceRemoveStopwords(new File("/Users/josericardoramos/Investigacao/libs/mallet-2.0.7/stoplists/en.txt"), "UTF-8", false, false, false) );
        pipeList.add( new TokenSequence2FeatureSequence() );
		instances = new InstanceList (new SerialPipes(pipeList));
	
		
		////
		try {
			loadClassifiers();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getTopics();
	}
	
	public void getTopics(){
		
		try{
			int i=0;
			
			while(true){
				getContext(findIndex(movie.getGenre(i).getName()),movie.getDescription());
				i++;
			}
		}catch(IndexOutOfBoundsException e){
			e.printStackTrace();
		}
	}
	
	public int findIndex(String genreName){
		for(int i=0;i<genres.length;i++){
			if(genres[i].compareTo(genreName.replace(" ","_"))==0){
				return i;
			}
		}
		
		return -1;
	}
	
	public void getContext(int genre,String description){
		
		ArrayList<TreeSet<IDSorter>> topicSortedWords = models.get(genre).getSortedWords();
        // Create a new instance named "test instance" with empty target and source fields.
        InstanceList testing = new InstanceList(instances.getPipe());
        testing.addThruPipe(new StringArrayIterator(new String[]{description}));

        TopicInferencer inferencer = models.get(genre).getInferencer();
        double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
        
        Alphabet dataAlphabet =  models.get(genre).getAlphabet();
        
        for(int i=0;i<testProbabilities.length;i++){
	        if(testProbabilities[i]>max){
	        	max=testProbabilities[i];
	        	Iterator<IDSorter> iterator = topicSortedWords.get(i).iterator();
	        	int rank = 0;
	            while (iterator.hasNext() && rank < 5) {
	                IDSorter idCountPair = iterator.next();
	                System.out.print(idCountPair.toString()+ " ");
	                rank++;
	            }
	            System.out.print("\n");
	        }
        }
	}
	
	public void loadClassifiers() throws FileNotFoundException, IOException, ClassNotFoundException {
		
        String mainFolder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/";
        models=new ArrayList<ParallelTopicModel>();
        
        for(int i=0;i<genres.length;i++){
        	 ParallelTopicModel model;
        	 File modelFile=new File(mainFolder+genres[i]+"/topic.model");
        	 ObjectInputStream ois = new ObjectInputStream (new FileInputStream (modelFile));
        	 model = (ParallelTopicModel) ois.readObject();
             ois.close();
             models.add(model);
        }
   
	}
}
