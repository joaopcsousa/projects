package info_extraction;

import info_extraction.entities.Movie;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Pattern;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.StringArrayIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;

public class TopicModellingVisualization {
	InstanceList instances;
	ParallelTopicModel model;
	Alphabet dataAlphabet;
	ArrayList<String> descriptions;
	ArrayList<Topic> topicsTerms;
	
	public TopicModellingVisualization(List<Movie> movies){
		topicsTerms=new ArrayList<Topic>();
		String[] descriptions=getDescriptions(movies);
		
		checkTopics(descriptions);
		
		//associateDescriptions(descriptions);
	}
	
	public String[] getDescriptions(List<Movie> movies){
		String[] descriptions=new String[movies.size()];
		
		int i=0;
		System.out.println(movies.size());
		for(Movie movie:movies){
			descriptions[i]=movie.getDescription();
			i++;
		}
		
		
		return descriptions;
	}
	
	public void checkTopics(String[] descriptions){
        // Begin by importing documents from text to feature sequences
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
        //ArrayList<String> topicWords;
        String content;
        // Pipes: lowercase, tokenize, remove stopwords, map to features
        pipeList.add( new CharSequenceLowercase() );
        pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
        pipeList.add( new TokenSequenceRemoveStopwords(new File("/Users/josericardoramos/Investigacao/libs/mallet-2.0.7/stoplists/en.txt"), "UTF-8", false, false, false) );
        pipeList.add( new TokenSequence2FeatureSequence() );

        instances = new InstanceList (new SerialPipes(pipeList));

        //Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
        instances.addThruPipe(new StringArrayIterator(descriptions)); // data, label, name fields

        // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
        //  Note that the first parameter is passed as the sum over topics, while
        //  the second is the parameter for a single dimension of the Dirichlet prior.
        int numTopics = 20;
        model = new ParallelTopicModel(numTopics, 1.0, 0.01);

        model.addInstances(instances);

        // Use two parallel samplers, which each look at one half the corpus and combine
        //  statistics after every iteration.
        model.setNumThreads(2);

        // Run the model for 50 iterations and stop (this is for testing only, 
        //  for real applications, use 1000 to 2000 iterations)
        model.setNumIterations(1500);
        try {
			model.estimate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Show the words and topics in the first instance

        // The data alphabet maps word IDs to strings
        dataAlphabet = instances.getDataAlphabet();
        ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
        
        for(int i=0;i<20;i++){
        	 Iterator<IDSorter> iterator = topicSortedWords.get(i).iterator();
             Topic newTopic=new Topic();
             int rank = 0;
             
             while (iterator.hasNext() && rank < 5) {
                 IDSorter idCountPair = iterator.next();
                 newTopic.words.add(dataAlphabet.lookupObject(idCountPair.getID())+" ");
                 rank++;
             }
             
             topicsTerms.add(newTopic);
        }
      
    }
}
