package info_extraction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.tartarus.snowball.ext.PorterStemmer;

import opennlp.tools.cmdline.PerformanceMonitor;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import info_extraction.entities.Movie;

public class OpenNLP {
	
	String[] sentences;
	String content;
	Tokenizer tokenizer;
	POSTaggerME tagger;
	
	public OpenNLP(Movie movie,Tokenizer tokenizer,POSTaggerME tagger){
		this.tokenizer=tokenizer;
		this.tagger=tagger;
		
		content="";
		
		String[] sentences=getSentences(movie.getDescription());
		
		for(int i=0;i<sentences.length;i++){
			try {
				POSTag(sentences[i]);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		}
		
	}
	
	public OpenNLP(String description,Tokenizer tokenizer,POSTaggerME tagger){
		this.tokenizer=tokenizer;
		this.tagger=tagger;
		
		content="";
		
		String[] sentences=getSentences(description);
		
		for(int i=0;i<sentences.length;i++){
			try {
				POSTag(sentences[i]);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	public String[] getSentences(String desciption){
		try {
			InputStream is;
			is = new FileInputStream("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-sent.bin");
			SentenceModel model;
		
			model = new SentenceModel(is);
		
			SentenceDetectorME sdetector = new SentenceDetectorME(model);
			String sentences[] = sdetector.sentDetect(desciption);
			
			return sentences;
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void POSTag(String phrase) throws IOException {
	 
		ObjectStream<String> lineStream = new PlainTextByLineStream(new StringReader(phrase));

		String line;
		int i;
		while ((line = lineStream.read()) != null) {
	 		i=0;
			String whitespaceTokenizerLine[] = tokenizer.tokenize(line);
			String[] tags = tagger.tag(whitespaceTokenizerLine);
			POSSample sample = new POSSample(whitespaceTokenizerLine, tags);
			for(String tag:sample.getTags()){

				//System.out.println(whitespaceTokenizerLine[i]+" "+tag);
				
				//if(tag.compareTo("NN")==0){					
					content+=whitespaceTokenizerLine[i]+" ";
				//}
				
				i++;
				
			}
			
			//System.out.println(names);
		}
	}
	
	public static String stemTerm (String term) {
	    PorterStemmer stemmer = new PorterStemmer();
	    stemmer.setCurrent(term);
	    stemmer.stem();
	    return stemmer.getCurrent();
	}
	
}
