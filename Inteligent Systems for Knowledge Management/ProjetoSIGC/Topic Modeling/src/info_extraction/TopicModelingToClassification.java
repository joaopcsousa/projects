package info_extraction;

import cc.mallet.util.*;
import cc.mallet.types.*;
import cc.mallet.classify.Classifier;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.*;
import cc.mallet.topics.*;
import info_extraction.entities.Genre;
import info_extraction.entities.Movie;
import info_extraction.entities.MovieCollection;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;

import org.tartarus.snowball.ext.PorterStemmer;

public class TopicModelingToClassification {
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em= emf.createEntityManager();
	List<Movie> movieList;
	List<Genre> genres;
	ArrayList<MovieCollection> moviesByGenre;
	ParallelTopicModel model;
	InstanceList instances;
	Tokenizer tokenizer;
	POSTaggerME tagger;
	
    public static void main(String[] args) throws Exception {
    	new TopicModelingToClassification();
    	
    }
    
    public void getModels(){
		try {
			//pos tagger
			POSModel model = new POSModelLoader().load(new File("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-pos-maxent.bin"));
			tagger = new POSTaggerME(model);
			
			//tokenizer
			InputStream is = new FileInputStream("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-token.bin");
			TokenizerModel model1 = new TokenizerModel(is);
			tokenizer = new TokenizerME(model1);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public TopicModelingToClassification(){
    	getModels();
    	moviesByGenre=new ArrayList<MovieCollection>();
    	findGenres();
    	findMovies();
    	topicsByGenre();
    	
    	for(MovieCollection genre:moviesByGenre){
    		System.out.println("A ver topicos de "+genre.name);
    		String[] it=findDescriptionGenre(genre);
    		topicModeling(it,genre.name,true);
    		it=findDescriptionNotGenre(genre);
    		topicModeling(it,genre.name,false);
    	}
    	

    	

    	//topicsByGenre();
    }
    
    public String[] findDescriptionGenre(MovieCollection genre){
    	
    	List<String> descriptions=new ArrayList<String>();
    	
    	for(Movie movie:movieList){
    		try{
    			int i=0;
    			boolean check=false;
    			
    			while(true){
    				if(movie.getGenre(i).getName().compareTo(genre.name)==0){
    					check=true;
    					break;
    				}
    				i++;
    			}
    			if(check){
    				descriptions.add((new OpenNLP(movie,tokenizer,tagger)).content);
    			}
    		}catch(IndexOutOfBoundsException e){
    			
    		}
    		//descriptions.add(movie.getDescription());
    	}
    	
    	String[] descriptionsArray=new String[descriptions.size()];
	    
    	//System.out.println("NUMERO DE FILMES: "+descriptionsArray.length);
    	for(int i=0;i<descriptions.size();i++){
    		descriptionsArray[i]=descriptions.get(i);
    	}
    	
	    return descriptionsArray;     
	}
    
    public String[] findDescriptionNotGenre(MovieCollection genre){
    	
    	List<String> descriptions=new ArrayList<String>();
    	
    	for(Movie movie:movieList){
    		int i=0;
			boolean check=false;
			
    		try{
    			
    			while(true){
    				if(movie.getGenre(i).getName().compareTo(genre.name)==0){
    					check=true;
    					break;
    				}
    				i++;
    			}
  
    		}catch(IndexOutOfBoundsException e){
    			if(!check){
    				descriptions.add((new OpenNLP(movie,tokenizer,tagger)).content);
    			}
    		}
    		//descriptions.add(movie.getDescription());
    	}
    	
    	String[] descriptionsArray=new String[descriptions.size()];
	    
    	//System.out.println("NUMERO DE FILMES: "+descriptionsArray.length);
    	for(int i=0;i<descriptions.size();i++){
    		descriptionsArray[i]=descriptions.get(i);
    	}
    	
	    return descriptionsArray;     
	}
    
    public void findGenres(){

    	String queryMoviesByGenre="SELECT s FROM Genre s";
    	Query query=(Query) em.createQuery(queryMoviesByGenre,Genre.class);

    	
    	genres=query.getResultList();
    	
	 }
    
    public void findMovies(){

    	String queryMoviesByGenre="SELECT s FROM Movie s";
    	Query query=(Query) em.createQuery(queryMoviesByGenre,Movie.class);

    	
    	movieList=query.getResultList();
    	
	 }
    
    public void topicsByGenre(){
    	for(Genre genre:genres){
    		
    		MovieCollection movieCollection=new MovieCollection();
    		movieCollection.name=genre.getName();
    		
    		//System.out.println("Checar genrero..."+ genre.getName());
    		for(Movie movie:movieList){
    			try{
    				int i=0;
    			
    				while(true){
    					if(genre.getName().compareTo(movie.getGenre(i).getName())==0){
    						movieCollection.movie.add(movie);
    					}
    					i++;
    				}
    			}catch(Exception e){
    				
    			}
    		}
    		//System.out.println(movieCollection.movie.size());
    		if(movieCollection.movie.size()>0){
    			moviesByGenre.add(movieCollection);
    			//findTopics(movieCollection);
    		}
    	}
    }
    
    public void findTopics(MovieCollection movieCollection){
    	 // Create a new instance named "test instance" with empty target and source fields.
    	
    	for(int i=0;i<movieCollection.movie.size();i++){
    		
	        InstanceList testing = new InstanceList(instances.getPipe());
	        testing.addThruPipe(new StringArrayIterator(new String[]{movieCollection.movie.get(i).getDescription()}));
	
	        TopicInferencer inferencer = model.getInferencer();
	        double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
	        
	        double max=0.0;
	        int pos=0;
	        
	        for(int a=0;a<testProbabilities.length;a++){
	        	
	        	
	        	if(testProbabilities[a]>max){
	        		max=testProbabilities[a];
	        		pos=a;
	        	}
	        	
	        }
	        //System.out.println("Topic "+pos+"("+max+")");
        
    	}
    }
    
    public void topicModeling(String[] iterator1,String genreName,boolean pos){
        // Begin by importing documents from text to feature sequences
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
        //ArrayList<String> topicWords;
        String content;
        // Pipes: lowercase, tokenize, remove stopwords, map to features
        pipeList.add( new CharSequenceLowercase() );
        pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
        pipeList.add( new TokenSequenceRemoveStopwords(new File("/Users/josericardoramos/Investigacao/libs/mallet-2.0.7/stoplists/en.txt"), "UTF-8", false, false, false) );
        pipeList.add( new TokenSequence2FeatureSequence() );

        instances = new InstanceList (new SerialPipes(pipeList));

        //Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
        instances.addThruPipe(new StringArrayIterator(iterator1)); // data, label, name fields

        // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
        //  Note that the first parameter is passed as the sum over topics, while
        //  the second is the parameter for a single dimension of the Dirichlet prior.
        int numTopics = 30;
        model = new ParallelTopicModel(numTopics, 1.0, 0.01);

        model.addInstances(instances);

        // Use two parallel samplers, which each look at one half the corpus and combine
        //  statistics after every iteration.
        model.setNumThreads(2);

        // Run the model for 50 iterations and stop (this is for testing only, 
        //  for real applications, use 1000 to 2000 iterations)
        model.setNumIterations(1500);
        try {
			model.estimate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Show the words and topics in the first instance

        // The data alphabet maps word IDs to strings
        Alphabet dataAlphabet = instances.getDataAlphabet();
        
        FeatureSequence tokens = (FeatureSequence) model.getData().get(0).instance.getData();
        LabelSequence topics = model.getData().get(0).topicSequence;
        
        
        Formatter out = new Formatter(new StringBuilder(), Locale.US);
        
        double[] topicDistribution = model.getTopicProbabilities(0);

        // Get an array of sorted sets of word ID/count pairs
        ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
        String folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/";
        saveClassifier(folder+genreName.replace(" ", "_")+"/topic.model",model);
        // Show top 5 words in topics with proportions for the first document
        for (int topic = 0; topic < numTopics; topic++) {
        	content="";
            Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
            
            out = new Formatter(new StringBuilder(), Locale.US);
            out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
            int rank = 0;
            while (iterator.hasNext() && rank < 5) {
                IDSorter idCountPair = iterator.next();
                content+=dataAlphabet.lookupObject(idCountPair.getID()).toString()+" ";
               // out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
                rank++;
            }
            saveTopicWords(content,topic,genreName,pos);
            System.out.println(out);
        }
          
    }
    
    public static void saveClassifier(String folder, ParallelTopicModel serializedFile){

            // The standard method for saving classifiers in                                                   
            //  Mallet is through Java serialization. Here we                                                  
            //  write the classifier object to the specified file.                                             
    		try{
	            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream (new File(folder)));
	            oos.writeObject (serializedFile);
	            oos.close();
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    }
    
    public static String stemTerm (String term) {
	    PorterStemmer stemmer = new PorterStemmer();
	    stemmer.setCurrent(term);
	    stemmer.stem();
	    return stemmer.getCurrent();
	}
    
    public void saveTopicWords(String topicWords,int topicNumber, String genreName,boolean pos){
    	String folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/";
    	
    	try
		{
    		FileWriter writer;
    		if(pos)
    			writer = new FileWriter(folder+genreName.replace(" ","_")+"/pos/"+topicNumber+"_2.txt");
    		else
    			writer = new FileWriter(folder+genreName.replace(" ","_")+"/neg/"+topicNumber+"_2.txt");
		   	writer.append(topicWords);
		    writer.flush();
		    writer.close();
		}
		catch(IOException e)
		{
		     e.printStackTrace();
		} 
    }

}