package info_extraction.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "REVIEWS_TABLE")
public class Review{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "review_id")
	private int id;
	private String author;
	@Column(length=10000)
	private String comment;
	private String rank;

	public Review(){
		
	}

	public String getAuthor() {
		return author;
	}


	public String getComment(){
		return comment;
	}
	

	public void setAuthor(String author) {
		this.author=author;
	}


	public void setComment(String comment){
		this.comment=comment;
	}
	
	public int getId() {
		return id;
	}

	public void setRank(String rank){
		this.rank=rank;
	}
	
	public String getRank() {
		return rank;
	}
}
