package info_extraction.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name = "MOVIES_TABLE")
public class Movie {
	@Id
	@Column(name = "movie_id")
	private int id;
	@ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_genres", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="genre_id")})
    @LazyCollection(LazyCollectionOption.FALSE)   
	private List<Genre> genres;
	@OneToMany(cascade = {CascadeType.ALL} )
    @LazyCollection(LazyCollectionOption.FALSE)   
	private List<Review> reviews;
	private double rank;
	private int year;
	@Column(length=10000)
	private String description;
	@ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_crews", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="crew_id")})
    @LazyCollection(LazyCollectionOption.FALSE)   
	private List<Crew> crews;
	@ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_actors", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="actor_id")})
    @LazyCollection(LazyCollectionOption.FALSE)   
	private List<Cast> casts;
	@ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_languages", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="language_id")})
    @LazyCollection(LazyCollectionOption.FALSE)   
	private List<Language> languages;
	
	
	private String title;
	
	public Movie(){
		reviews=new ArrayList<Review>();
	}
	
	public Movie(int id,String title,double rank,String description,int year){
		this.setId(id);
		this.setTitle(title);
		this.setRank(rank);
		this.setDescription(description);
		this.year=year;
		genres=new ArrayList<Genre>();
		reviews=new ArrayList<Review>();
		languages=new ArrayList<Language>();
		casts=new ArrayList<Cast>();
		crews=new ArrayList<Crew>();
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getRank() {
		return rank;
	}

	public void setRank(double rank) {
		this.rank = rank;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}
	
	public int getGenresSize() {
		return genres.size();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Genre getGenre(int i) {
		return genres.get(i);
	}

	public void setGenre(Genre genre) {
		genres.add(genre);
	}
	
	public void setReview(Review review) {
		reviews.add(review);
	}
	
	public int getReviewsSize() {
		return reviews.size();
	}
	
	public Review getReview(int i) {
		return reviews.get(i);
	}
	
	public void setLanguage(Language language) {
		languages.add(language);
	}
	
	public Language getLanguage(int i) {
		return languages.get(i);
	}
	
	public void setCrew(Crew crew) {
		crews.add(crew);
	}
	
	public Crew getCrew(int i) {
		return crews.get(i);
	}
	
	public Cast getActors(int i) {
		return casts.get(i);
	}

	public void setActor(Cast actor) {
		casts.add(actor);
	}
	
	public int getLanguageSize() {
		return languages.size();
	}
	
	public int getYear() {
		return year;
	}
	
	public int getCrewSize(){
		return crews.size();
	}
	
	public int getActorsSize(){
		return casts.size();
	}
}
