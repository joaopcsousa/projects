package info_extraction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.sys.SystemTDB;

import info_extraction.entities.*;

public class PopulateOWL {
	String nameSpace="http://www.owl-ontologies.com/creationOwl#";
	OntModel ontologyModel;
	List<Movie> movies;
	ArrayList<Integer> moviesTopic;
	ArrayList<Topic> topicsTerms;
	
	EntityManagerFactory emf;
	EntityManager em;
	
	public static void main(String[] args) {
		new PopulateOWL();
	}
	
	PopulateOWL(){
		emf=Persistence.createEntityManagerFactory("SIGC");
		em= emf.createEntityManager();
		String owlBaseFile="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/Ontologia/ontology.owl";
		nameSpace="http://www.owl-ontologies.com/creationOwl#";
		ontologyModel =  ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null); 
		ontologyModel.read(owlBaseFile, "RDF/XML-ABBREV"); 
		getMovies();
		TopicDistributionGlobal topicDistribution=new TopicDistributionGlobal(movies);
		moviesTopic=topicDistribution.moviesTopic;
		topicsTerms=topicDistribution.topicsTerms;
		populate();
		save();
		//teste();
		ontologyModel.close();
	}
	
	
	public void teste(){
		TDB.getContext().set(SystemTDB.symFileMode, "mapped");
		System.out.print(TDB.getContext().get(SystemTDB.symFileMode));
	}
	
	public void populate(){
		OntClass movieClass=ontologyModel.getOntClass(nameSpace+"Movie");
		OntClass characterClass=ontologyModel.getOntClass(nameSpace+"Character");
		OntClass companyClass=ontologyModel.getOntClass(nameSpace+"Company");
		OntClass genreClass=ontologyModel.getOntClass(nameSpace+"Genre");
		OntClass languageClass=ontologyModel.getOntClass(nameSpace+"Language");
		OntClass personClass=ontologyModel.getOntClass(nameSpace+"Person");
		OntClass jobClass=ontologyModel.getOntClass(nameSpace+"Job");
		OntClass crewClass=ontologyModel.getOntClass(nameSpace+"Crew");
		OntClass castClass=ontologyModel.getOntClass(nameSpace+"Cast");
		OntClass reviewClass=ontologyModel.getOntClass(nameSpace+"Review");
		
		Property classificationProperty = ontologyModel.getProperty(nameSpace+"hasClassification");
		Property nameProperty = ontologyModel.getProperty(nameSpace+"hasName");
		Property idProperty = ontologyModel.getProperty(nameSpace+"hasID");
		Property resumeProperty = ontologyModel.getProperty(nameSpace+"hasResume");
		Property yearProperty = ontologyModel.getProperty(nameSpace+"hasYear");
		Property reviewProperty = ontologyModel.getProperty(nameSpace+"hasReview");
		Property jobProperty = ontologyModel.getProperty(nameSpace+"hasJob");
		Property personProperty = ontologyModel.getProperty(nameSpace+"hasPerson");
		Property castProperty = ontologyModel.getProperty(nameSpace+"hasCast");
		Property characterProperty = ontologyModel.getProperty(nameSpace+"hasCharacter");
		Property genreProperty = ontologyModel.getProperty(nameSpace+"hasGenres");
		Property languageProperty = ontologyModel.getProperty(nameSpace+"hasSpokenLanguages");
		Property crewProperty = ontologyModel.getProperty(nameSpace+"hasCrew");
		Property commentProperty = ontologyModel.getProperty(nameSpace+"hasComment");
		Property topicProperty = ontologyModel.getProperty(nameSpace+"hasTopic");
		
		int i,a;
		
		for(i=0;i<movies.size();i++){
			Individual newMovie=movieClass.createIndividual(nameSpace+"Movie"+movies.get(i).getId());
			newMovie.addLiteral(idProperty, movies.get(i).getId());
			newMovie.addLiteral(nameProperty, movies.get(i).getTitle());
			newMovie.addLiteral(classificationProperty, movies.get(i).getRank());
			newMovie.addLiteral(resumeProperty, movies.get(i).getDescription());
			newMovie.addLiteral(yearProperty, movies.get(i).getYear());
			
			int movieTopic=moviesTopic.get(i);
			
			for(int word=0;word<topicsTerms.get(movieTopic).words.size();word++){
				newMovie.addLiteral(topicProperty,topicsTerms.get(movieTopic).words.get(word));
			}
			
			for(a=0;a<movies.get(i).getGenresSize();a++){
				Individual newGenre;
				if((newGenre=ontologyModel.getIndividual(nameSpace+"Genre"+movies.get(i).getGenre(a).getId()))==null){
					newGenre=genreClass.createIndividual(nameSpace+"Genre"+movies.get(i).getGenre(a).getId());
					newGenre.addLiteral(nameProperty,movies.get(i).getGenre(a).getName());
					newGenre.addLiteral(idProperty,movies.get(i).getGenre(a).getId());
				}
				newMovie.addProperty(genreProperty,newGenre);
			}
			
			for(a=0;a<movies.get(i).getActorsSize();a++){
				Individual newPerson;
				Individual newCharacter;
				Individual newCast;
				if((newCast=ontologyModel.getIndividual(nameSpace+"Cast"+movies.get(i).getActors(a).getPeople().getId()+"a"+movies.get(i).getActors(a).getCharacter().getId()))==null){
					if((newPerson=ontologyModel.getIndividual(nameSpace+"People"+movies.get(i).getActors(a).getPeople().getId()))==null){
						newPerson=personClass.createIndividual(nameSpace+"People"+movies.get(i).getActors(a).getPeople().getId());
						newPerson.addLiteral(nameProperty,movies.get(i).getActors(a).getPeople().getName());
						newPerson.addLiteral(idProperty,movies.get(i).getActors(a).getPeople().getId());
					}
					
					if((newCharacter=ontologyModel.getIndividual(nameSpace+"Character"+movies.get(i).getActors(a).getCharacter().getId()))==null){
						newCharacter=characterClass.createIndividual(nameSpace+"Character"+movies.get(i).getActors(a).getCharacter().getId());
						newCharacter.addLiteral(nameProperty,movies.get(i).getActors(a).getCharacter().getName());
						newCharacter.addLiteral(idProperty,movies.get(i).getActors(a).getCharacter().getId());
					}
					newCast=castClass.createIndividual(nameSpace+"Cast"+movies.get(i).getActors(a).getPeople().getId()+"a"+movies.get(i).getActors(a).getCharacter().getId());
					newCast.addProperty(personProperty,newPerson);
					newCast.addProperty(characterProperty,newCharacter);
				
				}
				newMovie.addProperty(castProperty,newCast);
			}
			
			for(a=0;a<movies.get(i).getCrewSize();a++){
				Individual newPerson;
				Individual newJob;
				Individual newCrew;
				if((newCrew=ontologyModel.getIndividual(nameSpace+"Crew"+movies.get(i).getCrew(a).getPeople().getId()+"a"+movies.get(i).getCrew(a).getJob().getId()))==null){
					if((newPerson=ontologyModel.getIndividual(nameSpace+"People"+movies.get(i).getCrew(a).getPeople().getId()))==null){
						newPerson=personClass.createIndividual(nameSpace+"People"+movies.get(i).getCrew(a).getPeople().getId());
						newPerson.addLiteral(nameProperty,movies.get(i).getCrew(a).getPeople().getName());
						newPerson.addLiteral(idProperty,movies.get(i).getCrew(a).getPeople().getId());
					}
					
					if((newJob=ontologyModel.getIndividual(nameSpace+"Job"+movies.get(i).getCrew(a).getJob().getId()))==null){
						newJob=jobClass.createIndividual(nameSpace+"Job"+movies.get(i).getCrew(a).getJob().getId());
						newJob.addLiteral(nameProperty,movies.get(i).getCrew(a).getJob().getName());
						newJob.addLiteral(idProperty,movies.get(i).getCrew(a).getJob().getId());
					}
					
					newCrew=crewClass.createIndividual(nameSpace+"Crew"+movies.get(i).getCrew(a).getPeople().getId()+"a"+movies.get(i).getCrew(a).getJob().getId());
					newCrew.addProperty(personProperty,newPerson);
					newCrew.addProperty(jobProperty,newJob);
				
				}
				newMovie.addProperty(crewProperty,newCrew);
			}
			
			
			
			for(a=0;a<movies.get(i).getLanguageSize();a++){
				Individual newLanguage;
				
				if((newLanguage=ontologyModel.getIndividual(nameSpace+"Language"+movies.get(i).getLanguage(a).getId()))==null){
					newLanguage=languageClass.createIndividual(nameSpace+"Language"+movies.get(i).getLanguage(a).getId());
					newLanguage.addLiteral(nameProperty,movies.get(i).getLanguage(a).getName());
					newLanguage.addLiteral(idProperty,movies.get(i).getLanguage(a).getId());
				}
				
				newMovie.addProperty(languageProperty,newLanguage);
			}
			
			for(a=0;a<movies.get(i).getReviewsSize();a++){
				Individual newReview;
			
				newReview=reviewClass.createIndividual(nameSpace+"Review"+movies.get(i).getReview(a).getId());
				newReview.addLiteral(nameProperty,movies.get(i).getReview(a).getAuthor());
				newReview.addLiteral(idProperty,movies.get(i).getReview(a).getId());
				newReview.addLiteral(reviewProperty,movies.get(i).getReview(a).getComment());
				
				double rank=0;
				
				if((movies.get(i).getReview(a).getRank()).indexOf("/")!=-1){
					rank=Double.parseDouble(movies.get(i).getReview(a).getRank().substring(0,(movies.get(i).getReview(a).getRank()).indexOf("/")));
				}else{
					if((movies.get(i).getReview(a).getRank()).indexOf("A")!=-1){
						rank=5;
					}else if((movies.get(i).getReview(a).getRank()).indexOf("B")!=-1){
						rank=4;
					}else if((movies.get(i).getReview(a).getRank()).indexOf("C")!=-1){
						rank=3;
					}else if((movies.get(i).getReview(a).getRank()).indexOf("D")!=-1){
						rank=2;
					}else if((movies.get(i).getReview(a).getRank()).indexOf("E")!=-1){
						rank=1;
					}
						
				}
				
				newReview.addLiteral(classificationProperty, rank);
				
				newMovie.addProperty(commentProperty,newReview);
			}
			
		}
		
	
	}
	
	public void save(){

		String directory = "/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/TDBDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.WRITE) ;
		
		 try {
		     Model model = dataset.getDefaultModel() ;
		     model.add(ontologyModel);
		     dataset.commit() ;

	    } finally { 
	        dataset.end() ; 
	    }
		
	}
	public void getMovies(){
		String query="SELECT s FROM Movie s ";
	   	Query query1=(Query) em.createQuery(query,Movie.class);
	    movies=query1.getResultList();
	}
}
