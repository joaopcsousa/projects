package info_extraction;

import org.tartarus.snowball.ext.PorterStemmer;

public class Teste {
	
	public static void main(String[] args) {
		System.out.println(stemTerm("computing "));
	}
	
	public static String stemTerm (String term) {
	    PorterStemmer stemmer = new PorterStemmer();
	    stemmer.setCurrent(term);
	    stemmer.stem();
	    return stemmer.getCurrent();
	}
}
