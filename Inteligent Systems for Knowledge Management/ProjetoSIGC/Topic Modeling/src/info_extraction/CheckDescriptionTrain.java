package info_extraction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import cc.mallet.classify.Classifier;
import cc.mallet.classify.Trial;
import cc.mallet.grmm.inference.MessageArray.Iterator;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.pipe.iterator.StringArrayIterator;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.Labeling;

public class CheckDescriptionTrain {
	ArrayList<Classifier> classifiers;
	String[] genres;
	Tokenizer tokenizer;
	POSTaggerME tagger;
	
	public static void main(String[] args) {
    	new CheckDescriptionTrain();
	}
	
	public CheckDescriptionTrain(){
		genres=new String[]{"Action","Adventure","Animation","Comedy","Crime","Drama","Family","Fantasy","Foreign","History","Horror","Indie","Music","Mystery","Romance","Science_Fiction","Suspense","Thriller","War"};
		
		classifiers=new ArrayList<Classifier>();
		try {
			loadClassifiers();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String descTest="Suffering short-term memory loss after a head injury, Leonard Shelby embarks on a grim quest to find the lowlife who murdered his wife in this gritty, complex thriller that packs more knots than a hangman's noose. To carry out his plan, Shelby snaps Polaroids of people and places, jotting down contextual notes on the backs of photos to aid in his search and jog his memory. He even tattoos his own body in a desperate bid to remember.";
		
		test(descTest);
	}
	
	public void test(String desc){
		getModels();
		
		String content=(new OpenNLP(desc,tokenizer,tagger)).content;
		for(int i=0;i<genres.length;i++){
			System.out.println("Classificacao em:"+genres[i]);
			try {
				StringArrayIterator ad=new StringArrayIterator(new String[]{desc});
				printLabelings(classifiers.get(i),ad);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void getModels(){
		try {
			//pos tagger
			POSModel model = new POSModelLoader().load(new File("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-pos-maxent.bin"));
			tagger = new POSTaggerME(model);
			
			//tokenizer
			InputStream is = new FileInputStream("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-token.bin");
			TokenizerModel model1 = new TokenizerModel(is);
			tokenizer = new TokenizerME(model1);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
	
	public void printLabelings(Classifier classifier, StringArrayIterator instance) throws IOException {
                                     
      
        // Classifier.classify() returns a Classification object                                           
        //  that includes the instance, the classifier, and the                                            
        //  classification results (the labeling). Here we only                                            
        //  care about the Labeling.                                                                       
		java.util.Iterator<Instance> instances =
	            classifier.getInstancePipe().newIteratorFrom(instance);
		
            Labeling labeling = classifier.classify(instances.next()).getLabeling();

            // print the labels with their weights in descending order (ie best first)                     

            for (int rank = 0; rank < labeling.numLocations(); rank++){
                System.out.print(labeling.getLabelAtRank(rank) + ":" +
                                 labeling.getValueAtRank(rank) + " ");
            }
            System.out.println();

        
    }
	
	public void loadClassifiers() throws FileNotFoundException, IOException, ClassNotFoundException {
		
        String mainFolder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/";
        
        for(int i=0;i<genres.length;i++){
        	 Classifier classifier;
        	 File model=new File(mainFolder+genres[i]+"/"+genres[i]+"_TOTAL.model");
        	 ObjectInputStream ois = new ObjectInputStream (new FileInputStream (model));
             classifier = (Classifier) ois.readObject();
             ois.close();
             classifiers.add(classifier);
        }
   
	}
	

}
