package info_extraction;

import info_extraction.entities.Genre;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import cc.mallet.classify.C45Trainer;
import cc.mallet.classify.Classifier;
import cc.mallet.classify.ClassifierTrainer;
import cc.mallet.classify.MaxEntTrainer;
import cc.mallet.classify.NaiveBayesTrainer;
import cc.mallet.classify.Trial;
import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.FeatureSequence2FeatureVector;
import cc.mallet.pipe.Input2CharSequence;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.PrintInputAndTarget;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.Target2Label;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceLowercase;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.FileIterator;
import cc.mallet.types.InstanceList;
import cc.mallet.util.Randoms;

public class DescriptionTrain {
	Pipe pipe;
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em = emf.createEntityManager();
	List<Genre> genresList;
	
	public void findGenres(){
		 String queryGenreNames="SELECT s FROM Genre s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Genre.class);
		 
		 genresList=query.getResultList();
	}
	
	public static Classifier trainClassifier(InstanceList trainingInstances) {

        // Here we use a maximum entropy (ie polytomous logistic regression)                               
        //  classifier. Mallet includes a wide variety of classification                                   
        //  algorithms, see the JavaDoc API for details.                                                   
		//ClassifierTrainer trainer = new MaxEntTrainer();
		ClassifierTrainer trainer = new NaiveBayesTrainer();
		//ClassifierTrainer trainer = new C45Trainer();
        return trainer.train(trainingInstances);
    }
	
	public DescriptionTrain(){
		 pipe = buildPipe();
		 findGenres();
	}
	
	public InstanceList readDirectory(File directory) {
        return readDirectories(new File[] {directory});
    }
	public InstanceList readDirectories(File[] directories) {
        
        // Construct a file iterator, starting with the 
        //  specified directories, and recursing through subdirectories.
        // The second argument specifies a FileFilter to use to select
        //  files within a directory.
        // The third argument is a Pattern that is applied to the 
        //   filename to produce a class label. In this case, I've 
        //   asked it to use the last directory name in the path.
        FileIterator iterator =new FileIterator(directories,new TxtFilter(),FileIterator.LAST_DIRECTORY);
       // ->>>>StringArrayIterator ad=new StringArrayIterator(new String[]{"ola"});
        // Construct a new instance list, passing it the pipe
        //  we want to use to process instances.
        InstanceList instances = new InstanceList(pipe);

        // Now process each instance provided by the iterator.
        instances.addThruPipe(iterator);

        return instances;
    }
	
	
	public Pipe buildPipe() {
        ArrayList pipeList = new ArrayList();

        // Read data from File objects
        pipeList.add(new Input2CharSequence("UTF-8"));

        // Regular expression for what constitutes a token.
        //  This pattern includes Unicode letters, Unicode numbers, 
        //   and the underscore character. Alternatives:
        //    "\\S+"   (anything not whitespace)
        //    "\\w+"    ( A-Z, a-z, 0-9, _ )
        //    "[\\p{L}\\p{N}_]+|[\\p{P}]+"   (a group of only letters and numbers OR
        //                                    a group of only punctuation marks)
        Pattern tokenPattern =Pattern.compile("[\\p{L}\\p{N}_]+");

        // Tokenize raw strings
        pipeList.add(new CharSequence2TokenSequence(tokenPattern));

        // Normalize all tokens to all lowercase
        pipeList.add(new TokenSequenceLowercase());

        // Remove stopwords from a standard English stoplist.
        //  options: [case sensitive] [mark deletions]
        pipeList.add(new TokenSequenceRemoveStopwords(false, false));

        // Rather than storing tokens as strings, convert 
        //  them to integers by looking them up in an alphabet.
        pipeList.add(new TokenSequence2FeatureSequence());

        // Do the same thing for the "target" field: 
        //  convert a class label string to a Label object,
        //  which has an index in a Label alphabet.
        pipeList.add(new Target2Label());

        // Now convert the sequence of features to a sparse vector,
        //  mapping feature IDs to counts.
        pipeList.add(new FeatureSequence2FeatureVector());

        // Print out the features and the label
        //pipeList.add(new PrintInputAndTarget());

        return new SerialPipes(pipeList);
    }
	
    public static void main(String[] args) {
    	
    	DescriptionTrain importer = new DescriptionTrain();
    	
    	for(Genre genre:importer.genresList){
    		InstanceList instances = importer.readDirectory(new File("/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/"+genre.getName().replace(" ", "_")+"/"));
        	//instances.save(new File("/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/sentiment_train/sentiment_train.file"));
    		if(!instances.isEmpty()){
    			System.out.println("Testar "+genre.getName());
    			//testTrainSplit(instances);
    			//Classifier classifier=trainClassifier(instances);
    		}else{
    			System.out.println("SALTOU "+genre.getName());
    			continue;
    		}
    	
    		//testTrainSplit(instances);
    		
    		Classifier classifier=trainClassifier(instances);
    		try {
	    	   saveClassifier(classifier,new File("/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/"+genre.getName().replace(" ", "_")+"/"+genre.getName().replace(" ", "_")+"_TOTAL1.model"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
    	}
    }
    
    public static void saveClassifier(Classifier classifier, File serializedFile)
            throws IOException {

            // The standard method for saving classifiers in                                                   
            //  Mallet is through Java serialization. Here we                                                  
            //  write the classifier object to the specified file.                                             

            ObjectOutputStream oos =
                new ObjectOutputStream(new FileOutputStream (serializedFile));
            oos.writeObject (classifier);
            oos.close();
        }
    
    class TxtFilter implements FileFilter {

        /** Test whether the string representation of the file 
         *   ends with the correct extension. Note that {@ref FileIterator}
         *   will only call this filter if the file is not a directory,
         *   so we do not need to test that it is a file.
         */
        public boolean accept(File file) {
            return file.toString().endsWith(".txt");
        }
    }
    
    public static Trial testTrainSplit(InstanceList instances) {

        int TRAINING = 0;
        int TESTING = 1;
        int VALIDATION = 2;

        // Split the input list into training (90%) and testing (10%) lists.                               
	// The division takes place by creating a copy of the list,                                        
	//  randomly shuffling the copy, and then allocating                                               
	//  instances to each sub-list based on the provided proportions.                                  

        InstanceList[] instanceLists = instances.split(new Randoms(),new double[] {0.9, 0.1, 0.0});

	// The third position is for the "validation" set,                                                 
        //  which is a set of instances not used directly                                                  
        //  for training, but available for determining                                                    
        //  when to stop training and for estimating optimal                                               
	//  settings of nuisance parameters.                                                               
	// Most Mallet ClassifierTrainers can not currently take advantage                                 
        //  of validation sets.                                                                            

        Classifier classifier = trainClassifier( instanceLists[TRAINING] );


        System.out.println("Accuracy: " + new Trial(classifier, instanceLists[TESTING]).getAccuracy());
        
        return null;
    }
}
