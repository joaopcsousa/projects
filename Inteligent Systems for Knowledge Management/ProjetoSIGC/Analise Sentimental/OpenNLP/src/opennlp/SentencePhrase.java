/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package opennlp;

/**
 *
 * intent = false -> neg
 * intent = true -> pos
 */
public class SentencePhrase {
    String noum ;
    String verb ;
    String adv ;
    String adj;
    boolean intent ;
    String suj="";
    double value=0.0;
    int ind;

    public SentencePhrase(String noum, String verb, String adv, String adj, int ind) {
        this.noum = noum;
        this.verb = verb;
        this.adv = adv;
        this.adj = adj;
        this.intent = true;
    }
}
