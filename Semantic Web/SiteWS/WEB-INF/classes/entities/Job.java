package entities;


public class Job {

	int id;

	String name;
	
	public Job(){
		
	}
	
	public Job(String name){
		this.name=name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		name = name;
	}
	
}
