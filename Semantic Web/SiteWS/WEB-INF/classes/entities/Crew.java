package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class Crew implements Serializable{

	private int id;
  
	private Job job;
 
	private People people;
	
	public Crew(){
		
	}
	
	public Crew(Job jobs,People people){
		this.people=people;
		this.job=jobs;

	}
	
	public Job getJob(){
		return job;
	}
	
	public People getPeople(){
		return people;
	}
}
