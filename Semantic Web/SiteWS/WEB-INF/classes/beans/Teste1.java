package beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sdb.store.Feature.Name;
import com.hp.hpl.jena.tdb.TDBFactory;

import entities.Genre;
import entities.Movie;


public class Teste1 {
	int i;
	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	Dataset dataset;
	
	
	public Teste1(){
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
    	dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
	}
	
	
	public ArrayList<Result> semanticSearch(ArrayList<String> classes,ArrayList<String> jobs,ArrayList<String> languages,ArrayList<String> rank,ArrayList<String> date,ArrayList<String> genres,ArrayList<String> other){

		int i;
		Movie movie=null;
		ArrayList<Movie> moviesList=null;
		String name="";
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		ArrayList<Tuple1> tuples=getEntities(other);
		ArrayList<String> queries=new ArrayList<String>();
		ArrayList<String> types=new ArrayList<String>();
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ";
			   
	 	String querieAux="";
	 	
	 	
	 	for(i=0;i<classes.size();i++){
	 		
	 		if(classes.get(i).matches("(m|M)ovie(s)?")){
	 			System.out.println("MATCHES FILMES");
	 			querieAux="SELECT DISTINCT ?id ?name WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie.";
	 			types.add("Movies");
	 			
	 		}else if(classes.get(i).matches("(c|C)haracter(s)?")){
	 			System.out.println("MATCHES Characters");
	 			querieAux="SELECT DISTINCT ?id ?name ?idCharacter ?nameCharacter WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasCast ?cast. ?cast nameSpace:hasCharacter ?character. ?character nameSpace:hasID ?idCharacter."
		 				+ "?character nameSpace:hasName ?nameCharacter. ?cast nameSpace:hasPerson ?person. ?person nameSpace:hasName ?personName.";
	 			types.add("Characters");
	 			
	 		}else if(classes.get(i).matches("(a|A)ctor(s)?")){
	 			System.out.println("MATCHES Actors");
	 			querieAux="SELECT DISTINCT ?id ?name ?idActor ?nameActor WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasCast ?cast. ?cast nameSpace:hasPerson ?person. ?person nameSpace:hasID ?idPerson."
		 				+ "?person nameSpace:hasName ?nameActor.";
	 			types.add("Actors");
	 			
	 		}else if(classes.get(i).matches("(g|G)enre(s)?")){
	 			System.out.println("MATCHES Genre");
	 			querieAux="SELECT DISTINCT ?id ?name ?idGenre ?nameGenre WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasGenres ?genre. ?genre nameSpace:hasName ?nameGenre. ?genre nameSpace:hasID ?idGenre.";
	 			types.add("Genres");
	 			
	 		}else if(classes.get(i).matches("(((y|Y)ear)|((D|d)ate))(s)?")){
	 			System.out.println("MATCHES date");
	 			querieAux="SELECT DISTINCT ?id ?name ?idYear WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasYear ?idYear.";
	 			types.add("Years");
	 			
	 		}else if(classes.get(i).matches("(r|R)ank(s)?")){
	 			System.out.println("MATCHES Rank");
	 			querieAux="SELECT DISTINCT ?id ?name ?nameRank WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasClassification ?nameRank.";
	 			types.add("Ranks");
	 			
	 		}else if(classes.get(i).matches("(c|C)ompan(y|ies)")){
	 			System.out.println("MATCHES company");
	 			querieAux="SELECT DISTINCT ?id ?name ?nameCompany WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasCompany ?company. ?company nameSpace:hasName ?nameCompany. ";
	 			types.add("Companies");
	 			
	 		}else if(classes.get(i).matches("(j|J)ob(s)?")){
	 			System.out.println("MATCHES jobs");
	 			querieAux="SELECT DISTINCT ?id ?name ?idJob ?nameJob WHERE {"
		 				+ "?cast ns:type nameSpace:Crew. ?cast nameSpace:hasPerson ?person. ?person nameSpace:hasName ?name."
		 				+ "?cast nameSpace:hasJob ?job. ?job nameSpace:hasName ?nameJob . ?job nameSpace:hasID ?idJob ."
		 				+ "?person nameSpace:hasID ?id. ";
	 			int check=0;
	 			for(int a=0;a<tuples.size();a++){
	 				if(tuples.get(a).type.substring(tuples.get(a).type.indexOf('#')+1, tuples.get(a).type.length()).compareTo("Person")==0){
		 				if(check==0){
		 					querieAux+="FILTER (regex(?name,'^"+tuples.get(a).name+"$','i')";
		 					check++;
		 				}else{
		 					querieAux+="|| regex(?name,'^"+languages.get(a)+"$','i')";
		 				}
		 			}
	 			}
	 			if(check>0){
 					querieAux+=").";
 				}
	 			
	 			types.add("Jobs");
	 			
	 		}

	 		
	 		queries.add(queryString+querieAux);
	 		
	 	}
	 	for(i=0;i<jobs.size();i++){
	 		System.out.println("???");
	 		//if(classes.get(i).matches("(((d|D)irector)|((p|P)roducer)|((o|O)riginal (m|M)usic (c|C)omposer)|((d|D)irector of (p|P)hotography)|((p|P)roduction (d|D)esign)|((c|C)asting)|((e|E)ditor)|((s|S)creenplay)|(Art Direction)|(Author)|(Executive Producer)|(Writer)|((M|m)usic)|((p|P)roduction Manager)|((N|n)ovel)|(Animation)|((S|s)pecial Effects)|((S|s)tory)|((M|m)usical)|((p|P)roduction Supervisor)|((O|o)riginal Story)|((c|C)reator))(s)?")){
	 			System.out.println("MATCHES Director");
	 			querieAux="SELECT DISTINCT ?id ?name ?idCrew ?nameCrew WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id ."
		 				+ " ?movie ns:type nameSpace:Movie . ?movie nameSpace:hasCrew ?crew . ?crew nameSpace:hasPerson ?person . ?person nameSpace:hasID ?idCrew ."
		 				+ " ?person nameSpace:hasName ?nameCrew . ?crew nameSpace:hasJob ?job . ?job nameSpace:hasName ?jobName . FILTER (regex(?jobName,'^"+jobs.get(i)+"$','i')) .";
	 		//}
	 		queries.add(queryString+querieAux);
	 		types.add("Jobs");
	 	}
	 	
	 	querieAux="";
	 	
	 		if(languages.size()>0){
	 			querieAux+="?movie nameSpace:hasSpokenLanguages ?language . ?language nameSpace:hasName ?nameLanguage  . FILTER (regex(?nameLanguage,'^"+languages.get(0)+"$','i')"; 
	 			
		 		for(int a=1;a<languages.size();a++){
		 			querieAux+=" || regex(?language,'^"+languages.get(a)+"$','i')";
		 		}
		 		querieAux+=").";
			}
	 		
	 		if(rank.size()>0){
	 			querieAux+="?movie nameSpace:hasClassification ?classification . FILTER (?classification = "+rank.get(0); 
	 			
		 		for(int a=1;a<rank.size();a++){
		 			querieAux+=" || ?rank = "+rank.get(a);
		 		}
		 		querieAux+=").";
			}
	 		
	 		if(date.size()>0){
	 			querieAux+="?movie nameSpace:hasYear ?year . FILTER (?year = "+date.get(0); 
	 			
		 		for(int a=1;a<date.size();a++){
		 			querieAux+=" || ?year = "+date.get(a);
		 		}
		 		querieAux+=").";
			}
	 		
	 		if(genres.size()>0){
	 			System.out.println("Entrou Genres");
	 			querieAux+="?movie nameSpace:hasGenres ?genre . ?genre  nameSpace:hasName ?nameGenres . FILTER (regex(?nameGenres,'^"+genres.get(0)+"$','i')"; 
	 			
	 			for(int a=1;a<genres.size();a++){
	 				querieAux+=" || regex(?nameGenres,'^"+genres.get(a)+"$','i')";
		 		}
	 			querieAux+="). ";
			}
	 		
	 		int numPerson=0;
	 		int numCompany=0;
	 		int numMovie=0;
	 		
	 		List<String> moviesIds=new ArrayList<String>();
	 		List<String> moviesIdsAux=new ArrayList<String>();
	 		PopulateLucene search=new PopulateLucene();
	 		
	 		String peopleQ="";
	 		String companyQ="";
	 		String movieQ="";
	 		int person=0;

	 		for(Tuple1 tuple:tuples){
	 			System.out.println("Entrou tuples "+tuple.name);
	 			List<Integer> res;
	 			if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Person")==0 && !classes.get(i).matches("(c|C)haracter(s)?")){
	 				peopleQ+=numPerson==0 ? "person:'"+tuple.name+"' " : "AND person:'"+tuple.name+"' ";
	 				numPerson++;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Company")==0){
	 				companyQ+=numCompany==0 ? "company:'"+tuple.name+"' " : "AND company:'"+tuple.name+"' ";
	 				numCompany++;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Movie")==0){
	 				movieQ+=numMovie==0 ? "title:'"+tuple.name+"' " : "AND title:'"+tuple.name+"' ";
	 				numMovie++;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Person")==0 && classes.get(i).matches("(c|C)haracter(s)?")){
	 				if(person==0){
	 					querieAux+=" FILTER (regex(?personName,'^"+tuple.name+"$','i')";
	 					person++;
	 				}else{
	 					querieAux+=" || regex(?personName,'^"+tuple.name+"$','i')";
	 				}
	 			}
	 			
	 		}
	 		if(person>0){
	 			querieAux+=")";
	 		}
	 		
	 		if(peopleQ.compareTo("")!=0){
	 			System.out.print(peopleQ);
		 		moviesIds=search.searchPeople(peopleQ);
	 		}if(companyQ.compareTo("")!=0){
	 			System.out.println("Entrou!");
		 		moviesIdsAux=search.searchCompanies(companyQ);
		 		
		 		if(moviesIds!=null && moviesIds.size()!=0){
			 		for(String index:moviesIds){
			 			if(moviesIdsAux.contains(index)){
			 				System.out.println("Ta a funcionar!");
			 			}else
			 				moviesIds.remove(index);
			 		}
		 		}else
		 			moviesIds=moviesIdsAux;
	 		}if(movieQ.compareTo("")!=0){
	 			moviesIdsAux=search.searchMovies(movieQ);
		 		
	 			if(moviesIds!=null && moviesIds.size()!=0){
			 		for(String index:moviesIds){
			 			if(moviesIdsAux.contains(index)){
			 				System.out.println("Ta a funcionar!");
			 			}else
			 				moviesIds.remove(index);
			 		}
	 			}else
		 			moviesIds=moviesIdsAux;
	 		}
		 		
	 		
		 	if(moviesIds.size()>0){
		 		querieAux+=" {?movie nameSpace:hasID "+moviesIds.get(0)+"}";
		 	}
		 		
		 	for(i=1;i< moviesIds.size() ;i++){
		 		querieAux+=" UNION {?movie nameSpace:hasID "+moviesIds.get(i)+"}";
		 	}
	 	
		 	
		 	
		 querieAux+="}";
	 	System.out.println(queryString);
	 	/*for(i=0;i<classes.size();i++){
	 		
	 	}*/
	 			
	 	ArrayList<Result> res=new ArrayList<Result>();		
	 	for(i=0;i<queries.size();i++){
	 		
	 		System.out.println(queries.get(i)+querieAux);
		    QueryExecution qExec = QueryExecutionFactory.create(queries.get(i)+querieAux, dataset) ;
		    ResultSet rs = qExec.execSelect() ;
		    String columnName;
			try {
					//moviesList=new ArrayList<Movie>();
					
					while(rs.hasNext()){
						Result resAux=new Result();
						resAux.type=types.get(i);
						QuerySolution row= rs.next();
						Iterator columns=row.varNames();
						System.out.println("Nova linha!");
						while (columns.hasNext()) {
							
						      RDFNode cell = row.get((columnName=columns.next().toString()));
						      
						      if(columnName.compareTo("name")==0){
						    	  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.name=name;
						      }else if(columnName.compareTo("id")==0){
						    	  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.id=Integer.parseInt(name);
						      }else if(columnName.matches("name(.)*")){
					    		  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.name1=name;
					    	  }else if(columnName.matches("id(.)*")){
					    		  String campoAux=cell.asLiteral().toString();
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.id1=Integer.parseInt(name);
					    	  }
					    	  System.out.println(name);
					      
					 }
					res.add(resAux);
				 }
				
		
			} finally { }

	 	}
		return res;
    }
	
	public ArrayList<Tuple1> getEntities(ArrayList<String> other){
		ArrayList<Tuple1> list=new ArrayList<Tuple1>();
		
		String name="";
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	
			   
	 		
	 	for(i=0;i<other.size();i++){
	 		 String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ";
	 		 
	 		queryString+="SELECT DISTINCT ?name ?type WHERE {?x nameSpace:hasName ?name"
	 				+ ". FILTER (regex(?name,\"(^|(The |A |An ))"+other.get(i)+"$\",'i')). ?x ns:type ?type}";
	 		
	 	
	 	

		 	System.out.println("->>>>>"+other.get(i));
		    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		    ResultSet rs = qExec.execSelect() ;
		    String columnName;

				while(rs.hasNext()){
			
					QuerySolution row= rs.next();
					Iterator columns=row.varNames();
					Tuple1 tuple=new Tuple1();
				
					while (columns.hasNext()) {
						
					      RDFNode cell = row.get((columnName=columns.next().toString()));

				    	  if(columnName.compareTo("name")==0){
				    		  String campoAux=cell.asLiteral().toString();
				    		  name=campoAux.substring(0,campoAux.indexOf('^'));
				    		  tuple.name=name;
				    	  }else if(columnName.compareTo("type")==0){
				    		  Node campoAux=cell.asNode();
				    		  name=campoAux.getURI();
				    		  tuple.type=name;
				    	  }
				    	 System.out.println(name);
					}
					list.add(tuple);
				}
			
			
		}
		
		return list;
	}
	
}

class Tuple2{
	String name;
	String type;
	
	Tuple2(){}
}