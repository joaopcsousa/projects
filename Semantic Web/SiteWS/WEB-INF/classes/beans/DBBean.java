package beans;

import java.util.Iterator;
import java.util.ArrayList;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.tdb.TDBFactory;

import entities.*;
import entities.Character;

public class DBBean{
	Dataset dataset;
	String nameSpace;
	String ns;
	
    public DBBean(){
		ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		nameSpace="http://www.owl-ontologies.com/creationOwl#";
    }


    public ArrayList<String> getGenres(){
    	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    	ArrayList<String> genresList=new ArrayList<String>();
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	    					"prefix ns: <"+ns+"> "+
					 		"SELECT DISTINCT ?name WHERE {?genre ns:type  nameSpace:Genre . ?genre nameSpace:hasName ?name.} ORDER BY asc(?name)";
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    try {
	    	while(rs.hasNext()){
	        	QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
					 String columnName=columns.next().toString();
				     RDFNode cell = row.get(columnName);
				     String campoAux=cell.asLiteral().toString();
				      if(columnName.compareTo("name")==0){
				    	  genresList.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
	    	}
	    } finally { qExec.close() ; }

	    return genresList;
    }

    

    /*public ArrayList<String> getCompanies(){
    	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    	ArrayList<String> genresList=new ArrayList<String>();
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	    					"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name WHERE {?company ns:type  nameSpace:Company . ?company nameSpace:hasName ?name}";
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    try {
	    	while(rs.hasNext()){
	        	QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
					 String columnName=columns.next().toString();
				     RDFNode cell = row.get(columnName);
				     String campoAux=cell.asLiteral().toString();
				      if(columnName.compareTo("name")==0){
				    	  genresList.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
	    	}
	    } finally { qExec.close() ; }

	    return genresList;
    }*/


    public ArrayList<Movie> browsing(String[] alfabeto,String[] categorias,String[] paises,String[] linguagens,int min,int max){
    	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    	ArrayList<String> genresList=new ArrayList<String>();
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		int i;
		Movie movie=null;
		ArrayList<Movie> moviesList=null;
		String name="";
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?id ?name ?year ?description ?rank WHERE {?movie nameSpace:hasName ?name."
	 			+ "?movie nameSpace:hasYear ?year . ?movie nameSpace:hasResume ?description . ?movie nameSpace:hasClassification ?rank . ?movie nameSpace:hasID ?id."
	 			+"?movie nameSpace:hasGenres ?genres. ?genres nameSpace:hasName ?nameGenres. ?movie nameSpace:hasCountry ?country . ?country nameSpace:hasName ?nameCountry."
	 			+"?movie nameSpace:hasSpokenLanguages ?linguagem. ?linguagem nameSpace:hasName ?nameLinguagem. FILTER (?rank >= "+min+" && ?rank<="+max+").";

	 	if(alfabeto!=null && alfabeto.length>0){
	 		queryString+="FILTER (regex(?name,'^"+alfabeto[0]+"(.)*','i') ";
		 	for(i=1;i<alfabeto.length;i++){
		 		queryString+="|| regex(?name,'^"+alfabeto[i]+"(.)*','i') ";
		 	}
	 		queryString+=").";

	 	}

	 	if(categorias!=null && categorias.length>0){
	 		queryString+="FILTER (regex(?nameGenres,'^"+categorias[0]+"','i') ";
		 	for(i=1;i<categorias.length;i++){
		 		System.out.println(categorias[i]);
		 		queryString+="|| regex(?nameGenres,'^"+categorias[i]+"','i') ";
		 	}
	 		queryString+=").";

	 	}

	 	if(paises!=null && paises.length>0){
	 		queryString+="FILTER (regex(?nameCountry,'"+paises[0]+"','i') ";
		 	for(i=1;i<paises.length;i++){
		 		queryString+="|| regex(?nameCountry,'"+paises[i]+"','i') ";
		 	}
	 		queryString+=").";

	 	}

	 	if(linguagens!=null && linguagens.length>0){
	 		queryString+="FILTER (regex(?nameLinguagem,'"+linguagens[0]+"','i') ";
		 	for(i=1;i<linguagens.length;i++){
		 		queryString+="|| regex(?nameLinguagem,'"+linguagens[i]+"','i') ";
		 	}
	 		queryString+=").";

	 	}

	 	queryString+="} ORDER BY desc(?rank)";

	 	
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    String columnName;
		try {
			if(rs.hasNext()){
				moviesList=new ArrayList<Movie>();
				while(rs.hasNext()){

					QuerySolution row= rs.next();
					Iterator columns=row.varNames();
					movie=new Movie();
					while (columns.hasNext()) {
						
					      RDFNode cell = row.get((columnName=columns.next().toString()));
					      if(cell.isLiteral()){
					    	  String campoAux=cell.asLiteral().toString();

					    	  if(columnName.compareTo("name")==0){
					    		  movie.setTitle(campoAux.substring(0,campoAux.indexOf('^')));
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    	  }else if(columnName.compareTo("year")==0){
					    		  movie.setYear(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
					    	  }else if(columnName.compareTo("description")==0){
					    		  movie.setDescription(campoAux.substring(0,campoAux.indexOf('^')));
					    	  }else if(columnName.compareTo("rank")==0){
					    	  	movie.setRank(Double.parseDouble(campoAux.substring(0,campoAux.indexOf('^'))));
					    	  }else{
					    	  	movie.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
					    	  }
				      }
				 }
			     getOthers1(movie,name);
			     moviesList.add(movie);
			 }
			}
	
		} finally { dataset.end() ; }

		return moviesList;
    }

    public ArrayList<String> getLanguages(){
    	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    	ArrayList<String> genresList=new ArrayList<String>();
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	    					"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name WHERE {?language ns:type  nameSpace:Language . ?language nameSpace:hasName ?name} ORDER BY asc(?name)";
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    try {
	    	while(rs.hasNext()){
	        	QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
					 String columnName=columns.next().toString();
				     RDFNode cell = row.get(columnName);
				     String campoAux=cell.asLiteral().toString();
				      if(columnName.compareTo("name")==0){
				    	  genresList.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
	    	}
	    } finally { qExec.close() ; }

	    return genresList;
    }

    public ArrayList<String> getCountries(){
    	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    	ArrayList<String> genresList=new ArrayList<String>();
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	    					"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name WHERE {?country ns:type  nameSpace:Country . ?country nameSpace:hasName ?name} ORDER BY asc(?name)";
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    try {
	    	while(rs.hasNext()){
	        	QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
					 String columnName=columns.next().toString();
				     RDFNode cell = row.get(columnName);
				     String campoAux=cell.asLiteral().toString();
				      if(columnName.compareTo("name")==0){
				    	  genresList.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
	    	}
	    } finally { qExec.close() ; }

	    return genresList;
    }
    
    public ArrayList<Movie> getMoviesName(String name){
    	String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		Movie movie=null;

		ArrayList<Movie> moviesList=null;


			/*String queryString ="SELECT * where{ ?x ?y ?z}";
		     QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		     ResultSet rs = qExec.execSelect() ;
		     for(i=0;rs.hasNext();i++);
		     
		     System.out.println(i);*/
		String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?id ?name ?year ?description ?rank WHERE {?movie nameSpace:hasName ?name  . FILTER (regex(?name,' "+name+"$','i') || regex(?name,'^"+name+" ','i') ||  regex(?name,' "+name+" ','i'))."
	 			+ "?movie nameSpace:hasYear ?year . ?movie nameSpace:hasResume ?description . ?movie nameSpace:hasClassification ?rank . ?movie nameSpace:hasID ?id}";
    	
		QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		ResultSet rs = qExec.execSelect() ;
		String columnName;
		try {
			if(rs.hasNext()){
				moviesList=new ArrayList<Movie>();
				while(rs.hasNext()){

					QuerySolution row= rs.next();
					Iterator columns=row.varNames();
					movie=new Movie();
					while (columns.hasNext()) {
						
					      RDFNode cell = row.get((columnName=columns.next().toString()));
					      if(cell.isLiteral()){
					    	  String campoAux=cell.asLiteral().toString();

					    	  if(columnName.compareTo("name")==0){
					    		  movie.setTitle(campoAux.substring(0,campoAux.indexOf('^')));
					    	  }else if(columnName.compareTo("year")==0){
					    		  movie.setYear(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
					    	  }else if(columnName.compareTo("description")==0){
					    		  movie.setDescription(campoAux.substring(0,campoAux.indexOf('^')));
					    	  }else if(columnName.compareTo("rank")==0){
					    	  	movie.setRank(Double.parseDouble(campoAux.substring(0,campoAux.indexOf('^'))));
					    	  }else{
					    	  	movie.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
					    	  }
				      }
				 }
			     getOthers(movie,name);
			     moviesList.add(movie);
			 }
			}
	
		} finally { dataset.end() ; }
		
	
		return moviesList;    
	}

	public ArrayList<Tuple> getRecomendation(int id){
		ArrayList<Tuple> lista=new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "
		 		+"prefix ns: <"+ns+"> "
		 		+"SELECT ?movie1name ?id ?rank  (COUNT(DISTINCT ?genres)*5 as ?countGenre) (COUNT(DISTINCT ?crew)*5 as ?countCrew) (COUNT(DISTINCT ?cast) as ?countCast) (COUNT(DISTINCT ?character)*8 as ?countCharacter) (?countGenre+?countCrew+?countCast+?countCharacter as ?total) WHERE {?movie nameSpace:hasID "+id+" . ?movie ns:type nameSpace:Movie . ?movie1 ns:type nameSpace:Movie"
					+" . { ?movie nameSpace:hasCrew ?crew . ?movie1 nameSpace:hasCrew ?crew . ?crew nameSpace:hasJob ?job . ?job nameSpace:hasName ?nameJob. FILTER (regex(?nameJob, 'Director')||regex(?nameJob, 'Novel')) } UNION {?movie nameSpace:hasCast ?cast . ?movie1 nameSpace:hasCast ?cast}"
					+ "UNION {?movie nameSpace:hasGenres ?genres . ?movie1 nameSpace:hasGenres ?genres} UNION {?movie nameSpace:hasCast ?cast1 . ?movie1 nameSpace:hasCast ?cast2 . ?cast1 nameSpace:hasCharacter ?character . ?cast2 nameSpace:hasCharacter ?character}"
					+ ". ?movie1 nameSpace:hasName ?movie1name . ?movie1 nameSpace:hasClassification ?rank . "
					+ "?movie1 nameSpace:hasID ?id . FILTER(NOT EXISTS { ?movie1 nameSpace:hasID "+id+"}) }  GROUP BY ?movie1name ?id ?rank ORDER BY desc(?total) LIMIT 10";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){
	    	int res=0;
    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();

			while(columns.hasNext()){
				String name;
				RDFNode cell = row.get((name=columns.next().toString()));
				String campoAux=cell.asLiteral().toString();
				if(name.compareTo("total")==0){
		    	    tuple.setId1(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
	    	    }else if(name.compareTo("movie1name")==0){
	    	    	tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));
	    	    }else if(name.compareTo("id")==0){
	    	    	tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
	    	    }else {
	    	    	System.out.println("->>"+campoAux.substring(0,campoAux.indexOf('^')));
	    	    	System.out.println("Passor");
	    	    	if(name.compareTo("countGenre")==0){
	    	    		res=Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^')));
	    	    		res/=5;
	    	    		if(res>0)
	    	    			tuple.description+=name+": "+res+" ";
	    	    	}else if(name.compareTo("countCrew")==0){
	    	    		res=Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^')));
	    	    		res/=5;
	    	    		if(res>0)
	    	    			tuple.description+=name+": "+res+" ";
	    	    	}else if(name.compareTo("countCharacter")==0){
	    	    		res=Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^')));
	    	    		res/=8;
	    	    		if(res>0)
	    	    			tuple.description+=name+": "+res+" ";
	    	    	}else if(name.compareTo("countCast")==0){
	    	    		res=Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^')));
	    	    		if(res>0)
	    	    			tuple.description+=name+": "+res+" ";
	    	    	}else{
	    	    		tuple.description+=name+": "+campoAux.substring(0,campoAux.indexOf('^'))+" ";
	    	    	}

	    	    	//System.out.println(tuple.description);
	    	    }
    		}
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}


	public ArrayList<Tuple> getCastNames(String name){    
		ArrayList<Tuple> lista=new ArrayList<Tuple>();
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?id ?name WHERE {?cast nameSpace:hasPerson ?person . ?cast ns:type nameSpace:Cast . ?person nameSpace:hasName ?name"
					 				+". FILTER (regex(?name,' "+name+"$','i') || regex(?name,'^"+name+" ','i') ||  regex(?name,' "+name+" ','i')) . ?person nameSpace:hasID ?id} ";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    try {
	    	 while(rs.hasNext()){
	    	 	Tuple tuple=new Tuple();

				 QuerySolution row= rs.next();
				 Iterator columns=row.varNames();

			     RDFNode cell = row.get(columns.next().toString());
		    	 String campoAux=cell.asLiteral().toString();
		    	 tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

		    	cell = row.get(columns.next().toString());
		    	campoAux=cell.asLiteral().toString();
				tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));

		    	 lista.add(tuple);
	
			} 
	    } finally { qExec.close() ; }
	    
	    return lista;
	}
	
	public ArrayList<String> getCharacterCastURI(String namePerson){
		ArrayList<String> lista= new ArrayList<String>();
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name WHERE {?character nameSpace:hasName ?name . ?character ns:type nameSpace:Character "
					 				+". ?cast nameSpace:hasPerson ?person . ?person  nameSpace:hasName ?namePerson FILTER(lcase(str(?namePerson)) = '"+namePerson.toLowerCase()+"') . ?cast nameSpace:hasCharacter ?character} LIMIT 3";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    try {
	    	 while(rs.hasNext()){

				 QuerySolution row= rs.next();
				 Iterator columns=row.varNames();
				 while (columns.hasNext()) {
					 String columnName=columns.next().toString();
				      RDFNode cell = row.get(columnName);
				      if(columnName.compareTo("name")==0){
				    	  Movie movie=new Movie();
				    	  String campoAux=cell.asLiteral().toString();
				    	  lista.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
			} 
	    } finally { qExec.close() ; }
	    
	    return lista;
	}
	
	public ArrayList<Tuple> getCrewNames(String name){    
		ArrayList<Tuple> crewsName=new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?id ?name WHERE {?crew nameSpace:hasPerson ?person . ?crew ns:type nameSpace:Crew . ?person nameSpace:hasName ?name . ?person nameSpace:hasID ?id"
					 				+". FILTER (regex(?name,' "+name+"$','i') || regex(?name,'^"+name+" ','i') ||  regex(?name,' "+name+" ','i'))} ";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    try {
	    	 while(rs.hasNext()){
	    	 	Tuple tuple=new Tuple();

				QuerySolution row= rs.next();
				Iterator columns=row.varNames();

		        RDFNode cell = row.get(columns.next().toString());
	    		String campoAux=cell.asLiteral().toString();
	    	    tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

	    	    cell = row.get(columns.next().toString());
	    		campoAux=cell.asLiteral().toString();
	    		tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
	    	
				      
	    	    crewsName.add(tuple);
			} 
	    } finally { qExec.close() ; }

	    return crewsName;
	    
	}
	
	public ArrayList<String> getJobCrewURI(String namePerson){
		ArrayList<String> jobs=new ArrayList<String>();
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name WHERE {?job nameSpace:hasName ?name . ?job ns:type nameSpace:Job "
					 				+".  ?cast nameSpace:hasJob ?job . ?cast nameSpace:hasPerson ?person. ?person  nameSpace:hasName ?namePerson FILTER(lcase(str(?namePerson)) = '"+namePerson.toLowerCase()+"') } LIMIT 3";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    try {
	    	 while(rs.hasNext()){

				 QuerySolution row= rs.next();
				 Iterator columns=row.varNames();
				 while (columns.hasNext()) {
				 	Job job=new Job();
					String columnName=columns.next().toString();
				    RDFNode cell = row.get(columnName);
				    if(columnName.compareTo("name")==0){
				    	String campoAux=cell.asLiteral().toString();
				    	campoAux=campoAux.substring(0,campoAux.indexOf('^'));
				    	jobs.add(campoAux);
				    }
				      
				 }
			} 
	    } finally { qExec.close() ; }
	    
	    return jobs;
	}

	public ArrayList<Tuple> getMoviesByGenresURI(int id){
		ArrayList<Tuple> lista=new ArrayList<Tuple>();
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "
					 		+"prefix ns: <"+ns+"> "
					 		+"SELECT DISTINCT ?name ?id WHERE {?movie nameSpace:hasName ?name . ?movie ns:type nameSpace:Movie "
	 						+". ?movie nameSpace:hasGenres ?genre . ?genre nameSpace:hasID "+id+" . ?movie nameSpace:hasID ?id}";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){

    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();
			
			RDFNode cell = row.get(columns.next().toString());

         	String campoAux=cell.asLiteral().toString();
    		tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

    	    cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}

	public ArrayList<Tuple> getJobsByPersonURI(int id){
		ArrayList<Tuple> lista=new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "
					 		+"prefix ns: <"+ns+"> "
					 		+"SELECT DISTINCT ?nameJob ?idJob ?movieName ?idMovie WHERE {?crew nameSpace:hasPerson ?person . ?crew ns:type nameSpace:Crew . ?job nameSpace:hasID ?idJob"
	 						+". ?crew nameSpace:hasJob ?job . ?job nameSpace:hasName ?nameJob . ?person nameSpace:hasID "+id+" . "
	 						+"?movie nameSpace:hasCrew ?crew . ?movie nameSpace:hasName ?movieName . ?movie nameSpace:hasID ?idMovie}";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){

    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();
			
			RDFNode cell = row.get(columns.next().toString());

         	String campoAux=cell.asLiteral().toString();
    		tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

    	    cell = row.get(columns.next().toString());

    	    campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setName1(campoAux.substring(0,campoAux.indexOf('^')));


         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
			
         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId1(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}

	public ArrayList<Tuple> getCharactersByPersonURI(int id){
		ArrayList<Tuple> lista=new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "
					 		+"prefix ns: <"+ns+"> "
					 		+"SELECT DISTINCT ?nameCharacter ?idCharacter ?movieName ?idMovie WHERE {?cast nameSpace:hasPerson ?person . ?cast ns:type nameSpace:Cast "
	 						+". ?cast nameSpace:hasCharacter ?character . ?character nameSpace:hasName ?nameCharacter . ?person nameSpace:hasID "+id+" . ?character nameSpace:hasID ?idCharacter"
	 						+".?movie nameSpace:hasCast ?cast . ?movie nameSpace:hasName ?movieName . ?movie nameSpace:hasID ?idMovie}";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){

    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();
			
			RDFNode cell = row.get(columns.next().toString());

         	String campoAux=cell.asLiteral().toString();
    		tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));

    	    cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setName1(campoAux.substring(0,campoAux.indexOf('^')));

         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId1(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}

	public ArrayList<Tuple> getGenresByMovieURI(int id){
		ArrayList<Tuple> lista= new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "
					 		+"prefix ns: <"+ns+"> "
					 		+"SELECT DISTINCT ?idGenre ?nameGenre WHERE {?movie nameSpace:hasGenres ?genre . ?genre ns:type nameSpace:Genre "
	 						+". ?genre nameSpace:hasName ?nameGenre . ?genre nameSpace:hasID ?idGenre . ?movie nameSpace:hasID "+id+"}";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){

    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();
			
			RDFNode cell = row.get(columns.next().toString());

         	String campoAux=cell.asLiteral().toString();
    		tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

    	    cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}

	public ArrayList<Tuple> getCrewByMovieURI(int id){
		ArrayList<Tuple> lista= new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "
					 		+"prefix ns: <"+ns+"> "
					 		+"SELECT DISTINCT ?idCrew ?nameCrew ?idJob ?nameJob WHERE {?movie nameSpace:hasCrew ?crew . ?crew ns:type nameSpace:Crew "
	 						+". ?crew nameSpace:hasPerson ?person . ?person nameSpace:hasName ?nameCrew . ?person nameSpace:hasID ?idCrew . ?movie nameSpace:hasID "+id
	 						+". ?crew nameSpace:hasJob ?job . ?job nameSpace:hasName ?nameJob . ?job nameSpace:hasID ?idJob} ORDER BY asc(?nameCrew)";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){

    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();
			
			RDFNode cell = row.get(columns.next().toString());

         	String campoAux=cell.asLiteral().toString();
    		tuple.setName1(campoAux.substring(0,campoAux.indexOf('^')));

    	    cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId1(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));

         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}

	public ArrayList<Tuple> getCastByMovieURI(int id){
		ArrayList<Tuple> lista= new ArrayList<Tuple>();

		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "
					 		+"prefix ns: <"+ns+"> "
					 		+"SELECT DISTINCT ?idCast ?nameCast ?idCharacter ?nameCharacter WHERE {?movie nameSpace:hasCast ?cast . ?cast ns:type nameSpace:Cast "
	 						+". ?cast nameSpace:hasPerson ?person . ?person nameSpace:hasName ?nameCast . ?person nameSpace:hasID ?idCast . ?movie nameSpace:hasID "+id
	 						+". ?cast nameSpace:hasCharacter ?character . ?character nameSpace:hasName ?nameCharacter . ?character nameSpace:hasID ?idCharacter} ORDER BY asc(?nameCast)";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){

    	 	Tuple tuple=new Tuple();
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();
			
			RDFNode cell = row.get(columns.next().toString());

         	String campoAux=cell.asLiteral().toString();
    		tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));

    	    cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setId1(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));

         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setName1(campoAux.substring(0,campoAux.indexOf('^')));

         	cell = row.get(columns.next().toString());

         	campoAux=cell.asLiteral().toString();
         	System.out.println(campoAux);
         	tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));
    	    
			      
			lista.add(tuple);
				 
		} 

		return lista;
	}

	public ArrayList<Tuple> getGenresNames(String name){
		ArrayList<Tuple> lista=new ArrayList<Tuple>();
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?id ?name WHERE {?genre nameSpace:hasName ?name . ?genre nameSpace:hasID ?id . ?genre ns:type nameSpace:Genre "
					 				+". FILTER (regex(?name,' "+name+"$','i') || regex(?name,'^"+name+" ','i') ||  regex(?name,' "+name+" ','i')) } ";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    String name1="";
	    try {
	    	 while(rs.hasNext()){
	    	 	System.out.println("ola");
	    	 	Tuple tuple=new Tuple();
				QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				
				RDFNode cell = row.get(columns.next().toString());

	         	String campoAux=cell.asLiteral().toString();
	    		tuple.setName(campoAux.substring(0,campoAux.indexOf('^')));

	    	    cell = row.get(columns.next().toString());

	         	campoAux=cell.asLiteral().toString();
	         	System.out.println(campoAux);
	         	tuple.setId(Integer.parseInt(campoAux.substring(0,campoAux.indexOf('^'))));
	    	    
				      
				lista.add(tuple);
				 
			} 
	    } finally { qExec.close() ; }
	    
	    return lista;
	}
	
	public ArrayList<String> getMoviesGenreURI(String genreName){
		ArrayList<String> lista=new ArrayList<String>();
		String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name ?rank WHERE {?movie nameSpace:hasName ?name . ?movie ns:type nameSpace:Movie "
					 				+". ?movie nameSpace:hasGenres ?genre . ?genre nameSpace:hasName ?namegenre FILTER(lcase(str(?namegenre)) = '"+genreName.toLowerCase()+"') . ?movie nameSpace:hasClassification ?rank } ORDER BY desc(?rank) LIMIT 3";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    Genre genre;
	    try {
	    	 while(rs.hasNext()){

				 QuerySolution row= rs.next();
				 Iterator columns=row.varNames();
				 while (columns.hasNext()) {
					 String columnName=columns.next().toString();
				      RDFNode cell = row.get(columnName);
				      if(columnName.compareTo("name")==0){
				    	  String campoAux=cell.asLiteral().toString();
				    	  lista.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
			} 
	    } finally { qExec.close() ; }
	    
	    return lista;
	} 

    public ArrayList<String> getMoviesNames(){
		ArrayList<String> names=new ArrayList<String>();
	    String queryString ="prefix nameSpace: <"+nameSpace+"> "+
					 			"prefix ns: <"+ns+"> "+
					 			"SELECT DISTINCT ?name WHERE {?genre nameSpace:hasName ?name . ?movie ns:type nameSpace:Movie . ?movie}";
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    try {
	    	 while(rs.hasNext()){

				 QuerySolution row= rs.next();
				 Iterator columns=row.varNames();
				 while (columns.hasNext()) {
				      RDFNode cell = row.get(columns.next().toString());
				      if(cell.isLiteral()){
				    	  String campoAux=cell.asLiteral().toString();
				    	  names.add(campoAux.substring(0,campoAux.indexOf('^')));
				      }
				      
				 }
			} 
	    } finally { qExec.close() ; }
	    
	    return names;
    }  

	public void getOthers(Movie movie,String name){
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";


			/*String queryString ="SELECT * where{ ?x ?y ?z}";
		     QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		     ResultSet rs = qExec.execSelect() ;
		     for(i=0;rs.hasNext();i++);
		     
		     System.out.println(i);*/
		String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?crew WHERE {?movie nameSpace:hasName ?name FILTER (lcase(str(?name)) = '"+name+"')."
	 			+ "?movie nameSpace:hasCrew ?crew }";
    	
		QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		ResultSet rs = qExec.execSelect() ;
		String columnName;
		try {
			while(rs.hasNext()){
				QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
	
				     RDFNode cell = row.get((columnName=columns.next().toString()));
				     getCrew(movie,cell.asResource().getURI());

			      }
			 }
		
		} finally { }
		
		queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?cast WHERE {?movie nameSpace:hasName ?name FILTER (lcase(str(?name)) = '"+name+"')."
	 			+ "?movie nameSpace:hasCast ?cast }";
		
		qExec = QueryExecutionFactory.create(queryString, dataset) ;
		rs = qExec.execSelect() ;
		try {
				while(rs.hasNext()){
				QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
				      RDFNode cell = row.get(columns.next().toString());
				      getCast(movie,cell.asResource().getURI());
				 }
			 }
		
		} finally { }
		

		queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?genre WHERE {?movie nameSpace:hasName ?name FILTER (lcase(str(?name)) = '"+name+"')."
	 			+ "?movie nameSpace:hasGenres ?genre }";
		
		qExec = QueryExecutionFactory.create(queryString, dataset) ;
		rs = qExec.execSelect() ;
		try {
				while(rs.hasNext()){
				QuerySolution row= rs.next();
				Iterator columns=row.varNames();
				while (columns.hasNext()) {
				      RDFNode cell = row.get(columns.next().toString());
				      getGenre(movie,cell.asResource().getURI());
				 }
			 }
		
		} finally { dataset.end() ;}
		
	}

	public void getOthers1(Movie movie,String name){
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";


			/*String queryString ="SELECT * where{ ?x ?y ?z}";
		     QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		     ResultSet rs = qExec.execSelect() ;
		     for(i=0;rs.hasNext();i++);
		     
		     System.out.println(i);*/
		String queryString;
    	
		QueryExecution qExec  ;
		ResultSet rs ;
		String columnName;
		Genre genre;
		String nameAux;

		queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?name1 WHERE {?movie nameSpace:hasName ?name . FILTER (regex(?name,\""+name+"\",'i'))." 
	 			+ "?movie nameSpace:hasGenres ?genre. ?genre nameSpace:hasName ?name1 }";
		
		qExec = QueryExecutionFactory.create(queryString, dataset) ;
		rs = qExec.execSelect() ;
		try {
				while(rs.hasNext()){
					QuerySolution row= rs.next();
					Iterator columns=row.varNames();
					genre=new Genre();
					while (columns.hasNext()) {
						 columnName=columns.next().toString();
					      RDFNode cell = row.get(columnName);
					      
					      if(columnName.compareTo("name1")==0){
					    	  nameAux=cell.asLiteral().toString();
					    	  genre.setName(nameAux.substring(0,nameAux.indexOf('^')));  
					      }
					
					}
					movie.setGenre(genre);
			 	}
		
		} finally { dataset.end() ;}
		
	}
	
	public void getGenre(Movie movie,String uri){

		Crew Genre;

		int id=Integer.parseInt(uri.substring(uri.indexOf("Genre")+5,uri.length()));
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		int i=0;
		

		String queryString= "prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?name WHERE {?genre nameSpace:hasID "+id+" . ?genre ns:type nameSpace:Genre . ?genre nameSpace:hasName ?name}";
		
		QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		ResultSet rs = qExec.execSelect() ;
		String columnName;
		
		Genre genre=new Genre();
		try {

			 QuerySolution row= rs.next();
			 Iterator columns=row.varNames();
			 while (columns.hasNext()) {
	
			     RDFNode cell = row.get(columns.next().toString());
			     String nome=cell.asLiteral().toString();
			     nome=nome.substring(0,nome.indexOf('^'));
			     genre.setName(nome);
			      
			 }
		
		}catch(Exception e){ dataset.end() ; }
		
		movie.setGenre(genre);

	}
	
	
	public void getCrew(Movie movie,String uri){

		int idPerson=Integer.parseInt(uri.substring(uri.indexOf("Crew")+4,uri.indexOf("a",uri.indexOf("Crew"))));
		int idJob=Integer.parseInt(uri.substring(uri.indexOf("a",uri.indexOf("Crew"))+1,uri.length()));
		Crew crew;

		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		int i=0;
		

		String queryString= "prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?name WHERE {?person nameSpace:hasID "+idPerson+" . ?person ns:type nameSpace:Person . ?person nameSpace:hasName ?name} ORDER BY asc(?name)";
		
		
		QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		ResultSet rs = qExec.execSelect() ;
		String columnName;
		
		People person=new People();
		try {

			 QuerySolution row= rs.next();
			 Iterator columns=row.varNames();
			 while (columns.hasNext()) {
	
			     RDFNode cell = row.get(columns.next().toString());
			      
			     String nome=cell.asLiteral().toString();
			     nome=nome.substring(0,nome.indexOf('^'));
			     person.setName(nome);
			      
			 }
		
		}catch(Exception e){}
		

		queryString= "prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?name WHERE {?job nameSpace:hasID "+idJob+" . ?job ns:type nameSpace:Job . ?job nameSpace:hasName ?name} ORDER BY asc(?name)";
		
		
		qExec = QueryExecutionFactory.create(queryString, dataset) ;
		rs = qExec.execSelect() ;
		Job job=new Job();
		try {

			 QuerySolution row= rs.next();
			 Iterator columns=row.varNames();
			 while (columns.hasNext()) {
	
			      RDFNode cell = row.get(columns.next().toString());
			      String name=cell.asLiteral().toString();
			      name=name.substring(0,name.indexOf('^'));
			      job.setName(name);
			      
			 }

		} finally { dataset.end() ; }
		
		crew=new Crew(job,person);
		
		movie.setCrew(crew);
	}
	
	public void getCast(Movie movie,String uri){
	
		int idPerson=Integer.parseInt(uri.substring(uri.indexOf("Cast")+4,uri.indexOf("a",uri.indexOf("Cast")+3)));
		int idCharacter=Integer.parseInt(uri.substring(uri.indexOf("a",uri.indexOf("Cast")+3)+1,uri.length()));
		Actor cast;
		String directory = "/Users/josericardoramos/Documents/workspace/ProjetoWS/TBDDatabase" ;
		Dataset dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		int i=0;
		

		String queryString= "prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?name WHERE {?person nameSpace:hasID "+idPerson+" . ?person ns:type nameSpace:Person . ?person nameSpace:hasName ?name}";
		
		
		QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		ResultSet rs = qExec.execSelect() ;
		String columnName;
		People person = new People();
		try {

			 QuerySolution row= rs.next();
			 Iterator columns=row.varNames();
			 while (columns.hasNext()) {
	
				 RDFNode cell = row.get(columns.next().toString());
			      String nome=cell.asLiteral().toString();
			      nome=nome.substring(0,nome.indexOf('^'));
			      person.setName(nome);
			      
			 }
		
		}catch(Exception e){}
		

		queryString= "prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
	 			"SELECT DISTINCT ?name WHERE {?character nameSpace:hasID "+idCharacter+" . ?job ns:type nameSpace:Character . ?character nameSpace:hasName ?name}";
		
		
		qExec = QueryExecutionFactory.create(queryString, dataset) ;
		rs = qExec.execSelect() ;
		Character character=new Character();
		try {

			 QuerySolution row= rs.next();
			 Iterator columns=row.varNames();
			 while (columns.hasNext()) {
	
			      RDFNode cell = row.get(columns.next().toString());
			      String nome=cell.asLiteral().toString();
			      nome=nome.substring(0,nome.indexOf('^'));
			      character.setName(nome);
			 }

		
		} finally { dataset.end() ; }
		
		cast=new Actor(person,character);
		
		movie.setActor(cast);
	}
		    
}