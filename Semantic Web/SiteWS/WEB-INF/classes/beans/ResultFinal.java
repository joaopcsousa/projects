package beans;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class ResultFinal{
	public int id;
 	public String name;
 	public String year;
    public ArrayList<Integer> idCharacter;
    public ArrayList<String> nameCharacter;
    public String rank;
    public ArrayList<Integer> idCompany;
    public ArrayList<String> nameCompany;
    public ArrayList<String> genres;
    public ArrayList<Integer> idJobs;
    public ArrayList<String> nameJobs;
    public ArrayList<Integer> idActors;
    public ArrayList<String> nameActors;
    public Set<String> type;
    public String description;

	ResultFinal(){
		idCharacter=new ArrayList<Integer>();
		nameCharacter=new ArrayList<String>();
		idCompany=new ArrayList<Integer>();
		nameCompany=new ArrayList<String>();
		genres=new ArrayList<String>();
		idJobs=new ArrayList<Integer>();
		nameJobs=new ArrayList<String>();
		idActors=new ArrayList<Integer>();
		nameActors=new ArrayList<String>();
		type=new HashSet<String>();
	}
    

}