<%@page import="beans.*,entities.*,java.util.ArrayList,java.util.Map"%>
<html>
<head>
  <script>
    function myFunction()
    {
      
    }
  </script>
</head>
<body>
<%
DBBean bean=new DBBean();


ArrayList<String> categoriesList=bean.getGenres();
ArrayList<String> countriesList=bean.getCountries();
ArrayList<String> languagesList=bean.getLanguages();

Map<java.lang.String,java.lang.String[]> map=request.getParameterMap();
String[] alfabeto=map.get("alfabeto");
String[] categoria=map.get("categoria");
String[] paises=map.get("country");
String[] lingua=map.get("language");

int min=0,max=10;
if(request.getParameter("min")!=null){
  min=Integer.parseInt(request.getParameter("min"));
}
if(request.getParameter("max")!=null){
  max=Integer.parseInt(request.getParameter("max"));
}
int a;
%>
<div class="hero-unit ">
	<h1 style="text-align:center">.:Movie Website:.</h1>
</div>
<div class="navbar navbar-inverse">
  <div class="navbar-inner">
    <ul class="nav">
      <li class="active"><a href="#">Home Page</a></li>
      <li class="divider-vertical"></li>
    </ul>
    <p style="text-align:right">
    	<form class="navbar-search pull-right" action="pesquisa.jsp" method="GET">
				<input name="semantica" type="text" class="search-query" placeholder="Search">
		</form>
  </div>
</div>

 <div class="span2">
      <form action="pesquisa.jsp" method="GET">

        <ul class="nav nav-list">  
              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Alfabeto</a>
                  <ul class="dropdown-menu">
<%
                    for(int i=0;i<=25;i++){
                        for(a=0;alfabeto!=null && a<alfabeto.length;a++){
                          if(alfabeto[a].compareTo(""+((char)('A'+i)))==0){
                            out.print("<div class='checkbox'> <label> <input type='checkbox' name='alfabeto' value='"+(char)('A'+i)+"' checked> "+(char)('A'+i)+"</input> </label> </div>");
                            break;
                          }

                        }
                        if(alfabeto==null || alfabeto.length==a){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' name='alfabeto' value='"+(char)('A'+i)+"'> "+(char)('A'+i)+"</input> </label> </div>");
                        }

          
                    }
%>
                     <ul class="pagination">
                        <li><a onclick="myFunction()">&laquo;</a></li>
                        <li><a onclick="changePage(1)">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                      </ul>
                  </ul>
              </li>

             <li class="divider"></li>  

              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Categorias </a>
                  <ul class="dropdown-menu">
<%
                    for(int i=0;i<categoriesList.size();i++){
                        for(a=0;categoria!=null && a<categoria.length;a++){
                          if(categoria[a].compareTo(categoriesList.get(i))==0){
                            out.print("<div class='checkbox'> <label> <input type='checkbox' name='categoria' value='"+categoriesList.get(i)+"' checked>"+categoriesList.get(i)+" </input></label> </div>");
                            break;
                          }
                        }
                        if(categoria==null || categoria.length==a){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' name='categoria' value='"+categoriesList.get(i)+"'>"+categoriesList.get(i)+" </input></label> </div>");
                        }
                    }
%>
                  </ul>
              </li>

              <li class="divider"></li> 
              
              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Pa&Iacute;s </a>
                  <ul class="dropdown-menu">
<%
                    for(int i=0;i<countriesList.size();i++){
                      for(a=0;paises!=null && a<paises.length;a++){
                        if(paises[a].compareTo(countriesList.get(i))==0){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' name='country' value='"+countriesList.get(i)+"' checked>"+countriesList.get(i)+" </input></label> </div>");
                          break;
                        }
                      }
                      if(paises==null || paises.length==a){
                        out.print("<div class='checkbox'> <label> <input type='checkbox' name='country' value='"+countriesList.get(i)+"'>"+countriesList.get(i)+" </input></label> </div>");
                      }
                    }
%>
                  </ul>
              </li>

              <li class="divider"></li>  
              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Linguagens</a>
                  <ul class="dropdown-menu">
<%
                    for(int i=0;i<languagesList.size();i++){
                      for(a=0;lingua!=null && a<lingua.length;a++){
                        if(lingua[a].compareTo(languagesList.get(i))==0){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' name='language' value='"+languagesList.get(i)+"' checked>"+languagesList.get(i)+" </input></label> </div>");
                          break;
                        }
                      }
                      if(lingua==null || lingua.length==a){
                        out.print("<div class='checkbox'> <label> <input type='checkbox' name='language' value='"+languagesList.get(i)+"'>"+languagesList.get(i)+" </input></label> </div>");
                      }
                    }
%>
                  </ul>
              </li>

              <li class="divider"></li>

              <a class="nav-header" tabindex="-1" href="#">Rank</a>
    
              <input name="min" type="text" class="input-small" value="<%=min%>">
              <input name="max" type="text" class="input-small" value="<%=max%>">
              
              <button type="submit"><i class="icon-search"></i></button>
                

            </form>
    
        </ul>  
    </div>
  </div>
</body>
</html>