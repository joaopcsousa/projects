<%@page import="beans.*,entities.*,java.util.ArrayList,java.util.Map"%>
<html>
<head>
  <style media="screen" type="text/css">
    #navlist li
    {
      display: inline;
      list-style-type: none;
      padding-right: 10px;
    }
  </style>

  <script>
    var url=window.location;
    var pages=new Array();
    var categorias=new Array();
    var pais=new Array();
    var linguagem=new Array();


    function codeAddress() {
         var searchString = window.location.search.substring(1), i, val, params = searchString.split("&");

         var countriesList=document.getElementById("countriesHidden").childNodes;
         var categoriesList=document.getElementById("categoriesHidden").childNodes;
         var languagesList=document.getElementById("languagesHidden").childNodes;


         for(var i=0;i<params.length;i++){
            if(params[i].split("=")[0]=="alfabeto"){
              pages[Math.floor(((params[i].split("=")[1].charCodeAt(0)-64)/6))+1]|=1<<((params[i].split("=")[1].charCodeAt(0)-64)%6);
             
            }

            if(params[i].split("=")[0]=="categoria"){
              for(var a=0;a<categoriesList.length;a++){
                if(categoriesList[a].innerText==params[i].split("=")[1]){
                  categorias[Math.floor(a/6)+1]|=1<<a%6;
                }
              }
            }

            if(params[i].split("=")[0]=="country"){
              for(var a=0;a<countriesList.length;a++){
                if(countriesList[a].innerText==params[i].split("=")[1]){
                  pais[Math.floor(a/6)+1]|=1<<a%6;
                }
              }
            }

            if(params[i].split("=")[0]=="language"){
              for(var a=0;a<languagesList.length;a++){
                if(languagesList[a].innerText==params[i].split("=")[1]){
                  linguagem[Math.floor(a/6)+1]|=1<<a%6;
                }
              }
            }

         }
    }
    window.onload = codeAddress;

    function changePageAlfabeto(numero)
    {

      var nodes=document.getElementById("alfabetoList").childNodes;
    
      for(var i=(numero-1)*6+1;i<=(numero-1)*6+6 && i<27;i++){
        //String.fromCharCode(64+i);
        if(pages[numero]==(pages[numero]|1<<(i-((numero-1)*6)))){
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectPage("+numero+","+(i-((numero-1)*6))+")'  name='alfabeto' value='"+String.fromCharCode(64+i)+"' checked>"+String.fromCharCode(64+i)+"</input></label></div>";
        }else{
          //alert((i-((numero-1)*6)));
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectPage("+numero+","+(i-((numero-1)*6))+")' name='alfabeto' value='"+String.fromCharCode(64+i)+"'>"+String.fromCharCode(64+i)+"</input></label></div>";
        }
      }

      if(i==27){
        for(i=27;i<27+4;i++)
           nodes[i-(numero-1)*6].innerHTML="";
      }
    }

    function changePageCategorias(numero)
    {

      var nodes=document.getElementById("categoriasList").childNodes;
      var nodes1=document.getElementById("categoriesHidden").childNodes;

      for(var i=(numero-1)*6+1;i<=(numero-1)*6+6 && i<32;i++){
        //String.fromCharCode(64+i);
        if(categorias[numero]==(categorias[numero]|1<<(i-((numero-1)*6)))){
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectCategoria("+numero+","+(i-((numero-1)*6))+")'  name='alfabeto' value='"+String.fromCharCode(64+i)+"' checked>"+nodes1[i].innerText+"</input></label></div>";
        }else{
         // alert("ola");
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectCategoria("+numero+","+(i-((numero-1)*6))+")' name='alfabeto' value='"+String.fromCharCode(64+i)+"'>"+nodes1[i].innerText+"</input></label></div>";
        }

      }

      if(i==32){
        for(i=32;i<37;i++)
           nodes[i-(numero-1)*6].innerHTML="";
      }

    }

    function selectCategoria(numero,indice)
    {

      var searchString = window.location.search.substring(1), i, val, params = searchString.split("&"),semantica=params[0].split("=");

      if(categorias[numero]==(categorias[numero]|1<<(i-((numero-1)*6)))){

        categorias[numero]=categorias[numero]|1<<indice;
      }else{
        categorias[numero]=categorias[numero]^1<<indice;
      }

    }

    function changePagePaises(numero)
    {

      var nodes=document.getElementById("paisesList").childNodes;
      var nodes1=document.getElementById("countriesHidden").childNodes;

      for(var i=(numero-1)*6+1;i<=(numero-1)*6+6 && i<52;i++){
        //String.fromCharCode(64+i);
        if(pais[numero]==(pais[numero]|1<<(i-((numero-1)*6)))){
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectPaises("+numero+","+(i-((numero-1)*6))+")'  name='alfabeto' value='"+String.fromCharCode(64+i)+"' checked>"+nodes1[i].innerText+"</input></label></div>";
        }else{
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectPaises("+numero+","+(i-((numero-1)*6))+")' name='alfabeto' value='"+String.fromCharCode(64+i)+"'>"+nodes1[i].innerText+"</input></label></div>";
        }

      }

      if(i==52){
        for(i=52;i<55;i++)
           nodes[i-(numero-1)*6].innerHTML="";
      }
    }

    function selectPaises(numero,indice)
    {

      var searchString = window.location.search.substring(1), i, val, params = searchString.split("&"),semantica=params[0].split("=");

      if(pais[numero]==(pais[numero]|1<<(i-((numero-1)*6)))){
        pais[numero]=pais[numero]|1<<indice;
      }else{
        pais[numero]=pais[numero]^1<<indice;
      }

    }

    function changePageLinguagem(numero)
    {

      var nodes=document.getElementById("linguagensList").childNodes;
      var nodes1=document.getElementById("languagesHidden").childNodes;

      for(var i=(numero-1)*6+1;i<=(numero-1)*6+6 && i<45;i++){
        //String.fromCharCode(64+i);
        if(linguagem[numero]==(linguagem[numero]|1<<(i-((numero-1)*6)))){
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectLinguagem("+numero+","+(i-((numero-1)*6))+")'  name='alfabeto' value='"+String.fromCharCode(64+i)+"' checked>"+nodes1[i].innerText+"</input></label></div>";
        }else{
          nodes[i-(numero-1)*6].innerHTML="<div class='checkbox'><label><input type='checkbox' onclick='selectLinguagem("+numero+","+(i-((numero-1)*6))+")' name='alfabeto' value='"+String.fromCharCode(64+i)+"'>"+nodes1[i].innerText+"</input></label></div>";
        }

      }

      if(i==45){
        for(i=45;i<50;i++)
           nodes[i-(numero-1)*6].innerHTML="";
      }
    }

    function selectLinguagem(numero,indice)
    {

      var searchString = window.location.search.substring(1), i, val, params = searchString.split("&"),semantica=params[0].split("=");

      if(linguagem[numero]==(linguagem[numero]|1<<(i-((numero-1)*6)))){
        linguagem[numero]=linguagem[numero]|1<<indice;
      }else{
        linguagem[numero]=linguagem[numero]^1<<indice;
      }

    }

    function selectPage(numero,indice)
    {

      var searchString = window.location.search.substring(1), i, val, params = searchString.split("&"),semantica=params[0].split("=");

      if(pages[numero]==(pages[numero]|1<<(i-((numero-1)*6)))){
        pages[numero]=pages[numero]|1<<indice;
      }else{
        pages[numero]=pages[numero]^1<<indice;
      }

    }

    function submit()
    {
       var linguagensArray=document.getElementById("languagesHidden").childNodes;
       var paisesArray=document.getElementById("countriesHidden").childNodes;
       var categoriasArray=document.getElementById("categoriesHidden").childNodes;
       var url="pesquisa.jsp";
       var content="";

       for(var i=1;i<6;i++){
          for(var a=1;a<=6 && a<27;a++){

            if(pages[i]==(pages[i]|1<<(a))){
              if(content==""){
                content="?alfabeto="+String.fromCharCode(64+((i-1)*6)+a);
              }else{
                content+="&alfabeto="+String.fromCharCode(64+((i-1)*6)+a);
              }
            }
          }
       }

       for(var i=1;i<7;i++){
          for(var a=1;a<=6 && a<32;a++){

            if(categorias[i]==(categorias[i]|1<<(a))){
              if(content==""){
                content="?categoria="+categoriasArray[((i-1)*6)+a].innerText;
                //content="?alfabeto=";
              }else{
                content+="&categoria="+categoriasArray[((i-1)*6)+a].innerText;
              }
            }
          }
       }

      for(var i=1;i<10;i++){
          for(var a=1;a<=6 && a<27;a++){

            if(pais[i]==(pais[i]|1<<(a))){
              if(content==""){
                content="?country="+paisesArray[((i-1)*6)+a].innerText;
                //content="?alfabeto=";
              }else{
                content+="&country="+paisesArray[((i-1)*6)+a].innerText;
              }
            }
          }
       } 

       for(var i=1;i<9;i++){
          for(var a=1;a<=6 && a<27;a++){

            if(linguagem[i]==(linguagem[i]|1<<(a))){
              if(content==""){
                content="?language="+linguagensArray[((i-1)*6)+a].innerText;
                //content="?alfabeto=";
              }else{
                 content+="&language="+linguagensArray[((i-1)*6)+a].innerText;
              }
            }
          }
       }       


       var min=document.getElementById('min').value;
       var max=document.getElementById('max').value;

       if(content==""){
          content+="?min="+min;
       }else{
          content+="&min="+min;
       }

       content+="&max="+max;

       window.location.href =url+content;
    }

  </script>
</head>
<body>
<%
DBBean bean=new DBBean();


ArrayList<String> categoriesList=bean.getGenres();
ArrayList<String> countriesList=bean.getCountries();
ArrayList<String> languagesList=bean.getLanguages();
languagesList.remove(0);

Map<java.lang.String,java.lang.String[]> map=request.getParameterMap();
String[] alfabeto=map.get("alfabeto");
String[] categoria=map.get("categoria");
String[] paises=map.get("country");
String[] lingua=map.get("language");

int min=0,max=10;


if(request.getParameter("min")!=null){
  min=Integer.parseInt(request.getParameter("min"));
}
if(request.getParameter("max")!=null){
  max=Integer.parseInt(request.getParameter("max"));
}
int a;
%>
<ul id="categoriesHidden" style="display: none;">
<% 
  for(a=0;a<categoriesList.size();a++){
    out.print("<li>"+categoriesList.get(a)+"</li>");
  }

%>
</ul>
<ul id="countriesHidden" style="display: none;">
<% 
  for(a=0;a<countriesList.size();a++){
    out.print("<li>"+countriesList.get(a)+"</li>");
  }

%>
</ul>
<ul id="languagesHidden" style="display: none;">
<% 
  for(a=0;a<languagesList.size();a++){
    out.print("<li>"+languagesList.get(a)+"</li>");
  }

%>
</ul>

<div class="hero-unit ">
	<h1 style="text-align:center">.:Movie Website:.</h1>
</div>
<div class="navbar navbar-inverse">
  <div class="navbar-inner">
    <ul class="nav">
      <li class="active"><a href="#">Home Page</a></li>
      <li class="divider-vertical"></li>
    </ul>
    <p style="text-align:right">
    	<form class="navbar-search pull-right" action="pesquisa.jsp" method="GET">
				<input name="semantica" type="text" class="search-query" placeholder="Search">
		</form>
  </div>
</div>

 <div class="span2">
      

        <ul class="nav nav-list">  
              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Alfabeto (26)</a>
                  <ul id="alfabetoList" class="dropdown-menu">
<%
                    for(int i=0;i<6;i++){
                        for(a=0;alfabeto!=null && a<alfabeto.length;a++){
                          if(alfabeto[a].compareTo(""+((char)('A'+i)))==0){
                            out.print("<div class='checkbox'> <label> <input type='checkbox' onclick='selectPage(1,"+(i+1)+")' name='alfabeto' value='"+(char)('A'+i)+"' checked> "+(char)('A'+i)+"</input> </label> </div>");
                            break;
                          }

                        }
                        if(alfabeto==null || alfabeto.length==a){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' onclick='selectPage(1,"+(i+1)+")' name='alfabeto' value='"+(char)('A'+i)+"'> "+(char)('A'+i)+"</input> </label> </div>");
                        }

          
                    }
%>
           
                      <ul class="paginationAlfabeto" id="navlist">
                       
                        <li><a onclick="changePageAlfabeto(1)">1</a></li>
                        <li><a onclick="changePageAlfabeto(2)">2</a></li>
                        <li><a onclick="changePageAlfabeto(3)">3</a></li>
                        <li><a onclick="changePageAlfabeto(4)">4</a></li>
                        <li><a onclick="changePageAlfabeto(5)">5</a></li>
                      
                      </ul>
                  </ul>
              </li>

             <li class="divider"></li>  

              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Categorias (<% out.print(categoriesList.size()); %>)</a>
                  <ul id="categoriasList" class="dropdown-menu">
<%
                    for(int i=0;i<6;i++){
                        for(a=0;categoria!=null && a<categoria.length;a++){
                          if(categoria[a].compareTo(categoriesList.get(i))==0){
                            out.print("<div class='checkbox'> <label> <input type='checkbox' name='categoria' onclick='selectCategoria(1,"+(i+1)+")' value='"+categoriesList.get(i)+"' checked>"+categoriesList.get(i)+" </input></label> </div>");
                            break;
                          }
                        }
                        if(categoria==null || categoria.length==a){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' onclick='selectCategoria(1,"+(i+1)+")' name='categoria' value='"+categoriesList.get(i)+"'>"+categoriesList.get(i)+" </input></label> </div>");
                        }
                    }
%>
                      <ul class="paginationCategorias" id="navlist">
                   
                        <li><a onclick="changePageCategorias(1)">1</a></li>
                        <li><a onclick="changePageCategorias(2)">2</a></li>
                        <li><a onclick="changePageCategorias(3)">3</a></li>
                        <li><a onclick="changePageCategorias(4)">4</a></li>
                        <li><a onclick="changePageCategorias(5)">5</a></li>
                        <li><a onclick="changePageCategorias(6)">6</a></li>
                      </ul>
                  </ul>
              </li>

              <li class="divider"></li> 
              
              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Pa&Iacute;s (<% out.print(countriesList.size()); %>)</a>
                  <ul id="paisesList" class="dropdown-menu">
<%
                    for(int i=0;i<6;i++){
                      for(a=0;paises!=null && a<paises.length;a++){
                        if(paises[a].compareTo(countriesList.get(i))==0){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' name='country' onclick='selectPaises(1,"+(i+1)+")' value='"+countriesList.get(i)+"' checked>"+countriesList.get(i)+" </input></label> </div>");
                          break;
                        }
                      }
                      if(paises==null || paises.length==a){
                        out.print("<div class='checkbox'> <label> <input type='checkbox' name='country' onclick='selectPaises(1,"+(i+1)+")' value='"+countriesList.get(i)+"'>"+countriesList.get(i)+" </input></label> </div>");
                      }
                    }
%>
                      <ul class="paginationPaises" id="navlist">
                   
                        <li><a onclick="changePagePaises(1)">1</a></li>
                        <li><a onclick="changePagePaises(2)">2</a></li>
                        <li><a onclick="changePagePaises(3)">3</a></li>
                        <li><a onclick="changePagePaises(4)">4</a></li>
                        <li><a onclick="changePagePaises(5)">5</a></li>
                        <li><a onclick="changePagePaises(6)">6</a></li>
                        <li><a onclick="changePagePaises(7)">7</a></li>
                        <li><a onclick="changePagePaises(8)">8</a></li>
                        <li><a onclick="changePagePaises(9)">9</a></li>
                      </ul>
                  </ul>
              </li>

              <li class="divider"></li>  
              <li class="dropdown-submenu">
                <a class="nav-header" tabindex="-1" href="#">Linguagens (30)</a>
                  <ul id="linguagensList" class="dropdown-menu">
<%
                    for(int i=0;i<6;i++){
                      for(a=0;lingua!=null && a<lingua.length;a++){
                        if(lingua[a].compareTo(languagesList.get(i))==0){
                          out.print("<div class='checkbox'> <label> <input type='checkbox' onclick='selectLinguagem(1,"+(i+1)+")' name='language' value='"+languagesList.get(i)+"' checked>"+languagesList.get(i)+" </input></label> </div>");
                          break;
                        }
                      }
                      if(lingua==null || lingua.length==a){
                        out.print("<div class='checkbox'> <label> <input type='checkbox' name='language' onclick='selectLinguagem(1,"+(i+1)+")' value='"+languagesList.get(i)+"'>"+languagesList.get(i)+" </input></label> </div>");
                      }
                    }
%>

                    <ul class="paginationLinguagem" id="navlist">
                   
                        <li><a onclick="changePageLinguagem(1)">1</a></li>
                        <li><a onclick="changePageLinguagem(2)">2</a></li>
                        <li><a onclick="changePageLinguagem(3)">3</a></li>
                        <li><a onclick="changePageLinguagem(4)">4</a></li>
                        <li><a onclick="changePageLinguagem(5)">5</a></li>

                      </ul>
                  </ul>
              </li>

              <li class="divider"></li>

              <a class="nav-header" tabindex="-1" href="#">Rank</a>
    
              <input name="min" id="min" type="text" class="input-small" value="<%=min%>">
              <input name="max" id="max" type="text" class="input-small" value="<%=max%>">
              
              <button onclick="submit()"><i class="icon-search"></i></button>
                

 
    
        </ul>  
    </div>
  </div>
</body>
</html>