<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:.</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <style type="text/css">
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
    </style>
  </head>

<body>

   <jsp:include page="header.jsp"></jsp:include>
   <%@page import="beans.*,entities.*,java.util.ArrayList"%>
<%
    DBBean bean=new DBBean();
    ArrayList<Tuple> aux=new ArrayList<Tuple>();
    if(request.getParameter("p")!=null){
%>
<div class="span10">
<b>Movie Name:<%=request.getParameter("name")%> </b>
<br><br>
<b>Genre : </b><br>
<% 
  aux=bean.getGenresByMovieURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<a href='genre.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a><br>");
%>
<br>
<b>Crew :</b>
<br> 
<% 
  aux=bean.getCrewByMovieURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<a href='individual.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a>(<a href='job.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a>)"+"<br>");
%>
<br>
<b>Cast :</b>
<br> 
<% 
  aux=bean.getCastByMovieURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<a href='individual.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a>(<a href='character.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a>)"+"<br>");
%>

<br>
<b>Related :</b>
<br> 
<% 
  aux=bean.getRecomendation(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<a href='movie.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a>("+aux.get(i).description+")"+"<br>");

  }

%>
</div>
</body>
</html>