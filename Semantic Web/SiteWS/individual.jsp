<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:.</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <style type="text/css">
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
    </style>
  </head>

<body>
   <jsp:include page="header.jsp"></jsp:include>
   <%@page import="beans.*,entities.*,java.util.ArrayList"%>
<%
    DBBean bean=new DBBean();
    ArrayList<Tuple> aux=new ArrayList<Tuple>();
    if(request.getParameter("p")!=null){
%>

<b>Person Name:<%=request.getParameter("name")%> </b>
<br><br>
<b></b>Crew Jobs : </b><br>
<% 
  aux=bean.getJobsByPersonURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<a href='job.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a>(<a href='movie.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a>)"+"<br>");
%>


<br><br>
<b></b>Cast Jobs : </b><br>
<% 
  aux=bean.getCharactersByPersonURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<a href='character.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a>(<a href='movie.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a>)"+"<br>");
%>


<%
    }

%>
</body>
</html>