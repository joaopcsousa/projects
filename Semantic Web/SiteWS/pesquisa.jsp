<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:.</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <style type="text/css">
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
    </style>
  </head>

<body>
   <jsp:include page="header.jsp"></jsp:include>
   <%@page import="beans.*,entities.*,java.util.ArrayList,java.util.Map"%>
<%

  DBBean bean=new DBBean();
  SemanticSearch semSearch=new SemanticSearch();
  /*int size=0;
  ArrayList<Movie> listaFilmes=new ArrayList<Movie>();
  ArrayList<Tuple> listaGeneros=new ArrayList<Tuple>();
  ArrayList<Tuple> listaCrew=new ArrayList<Tuple>();
  ArrayList<Tuple> listaCast=new ArrayList<Tuple>();
  ArrayList<String> listaAux=new ArrayList<String>();
  /*if(request.getParameter("pesquisa")!=null){
    listaFilmes=bean.getMoviesName(request.getParameter("pesquisa"));
    listaGeneros=bean.getGenresNames(request.getParameter("pesquisa"));
    listaCrew=bean.getCrewNames(request.getParameter("pesquisa"));
    listaCast=bean.getCastNames(request.getParameter("pesquisa"));

  }*/
  Map<java.lang.String,java.lang.String[]> map=request.getParameterMap();

  ArrayList<Movie> listaFilmes=null;
  ArrayList<ResultFinal> listaRes=null;

  if(request.getParameter("alfabeto")!=null || map.get("categoria")!=null || map.get("country")!=null || map.get("language")!=null){
    out.println("ola");
    listaFilmes=bean.browsing(map.get("alfabeto"), map.get("categoria"),map.get("country"), map.get("language"),Integer.parseInt(request.getParameter("min")),Integer.parseInt(request.getParameter("max")));
    out.println("ola");
%>
    <div class="span10">
      <h3>Movies</h3>
      

      <table class="table table-striped">
        <thead>
          <tr>
          <td><b>Title</b></td>
          <td><b>Rank</b></td>
          <td><b>Genres</b></td>
          </tr>
        </thead>
        <tbody>

    <% 
      for(int i=0;listaFilmes!=null && i<listaFilmes.size();i++){
    %>
        <tr>
          <td><a href="movie.jsp?name=<%=listaFilmes.get(i).getTitle()%>&p=<%=listaFilmes.get(i).getId()%>"><%=listaFilmes.get(i).getTitle()%></a></td>
          <td><%=listaFilmes.get(i).getRank()%></td>
          <td>
    <%
          for(int a=0;a<listaFilmes.get(i).getGenresSize();a++){
            out.println(listaFilmes.get(i).getGenre(a).getName());
            if(a!=listaFilmes.get(i).getGenresSize()-1)
              out.println(",");
          }
    %>
          </td>
  
        </tr>
    <%
      }
    %>
      </tbody>
      </table>

    </div>
<%  
  }else if(request.getParameter("semantica")!=null){
    listaRes=semSearch.searcher(request.getParameter("semantica"));
    out.println("<div class=\"span10\"><h4>Results for: "+request.getParameter("semantica")+" ("+listaRes.size()+")</h4>");
    if(listaRes.size()>0){
%>
    <table class="table table-striped">  
      <thead>  
          <tr> 
            <th>Name</th>

<%  
       
            out.println("!!!!!!"+listaRes.get(0).type.size());
            for(String type: listaRes.get(0).type){

              if(type.compareTo("Year")==0){
                out.print("<th>Year</th>");
              }else if(type.compareTo("Rank")==0){
                out.print("<th>Rank</th>");
              }else if(type.compareTo("Companies")==0){
                out.print("<th>Companies</th>");
              }else if(type.compareTo("Genres")==0){
                out.print("<th>Genres</th>");
              }else if(type.compareTo("Actors")==0){
                out.print("<th>Actors</th>");
              }else if(type.compareTo("Characters")==0){
                out.print("<th>Characters</th>");
              }else if(type.compareTo("Jobs")==0){
                out.print("<th>Jobs</th>");
              }
            }
        out.print("<th></th>");
        out.print("</thead><tbody>");

        for(int a=0;a<listaRes.size();a++){
              out.println("<tr>");
              out.print("<td><a href=\"movie.jsp?name="+listaRes.get(a).name+"&p="+listaRes.get(a).id+"\">"+listaRes.get(a).name+"</a></td>");
              for(String type: listaRes.get(0).type){

                if(type.compareTo("Year")==0){
                  out.print("<td>"+listaRes.get(a).year+"</td>");
                }else if(type.compareTo("Rank")==0){
                  out.print("<td>"+listaRes.get(a).rank+"</td>");
                }else if(type.compareTo("Genres")==0){
                  out.print("<td>");
                  for(int e=0;e<listaRes.get(a).genres.size();e++){
                    out.print(listaRes.get(a).genres.get(e)+"</br>");
                  }
                  out.print("</td>");
                }else if(type.compareTo("Companies")==0){
                  out.print("<td>");
                  for(int e=0;e<listaRes.get(a).nameCompany.size();e++){
                    out.print(listaRes.get(a).nameCompany.get(e)+"</br>");
                  }
                  out.print("</td>");
                }else if(type.compareTo("Actors")==0){
                   out.print("<td>");
                  for(int e=0;e<listaRes.get(a).nameActors.size();e++){
                    out.print(listaRes.get(a).nameActors.get(e)+"</br>");
                  }
                  out.print("</td>");
                }else if(type.compareTo("Characters")==0){
                  out.print("<td>");
                  for(int e=0;e<listaRes.get(a).nameCharacter.size();e++){
                    out.print(listaRes.get(a).nameCharacter.get(e)+"</br>");
                  }
                  out.print("</td>");
                }else if(type.compareTo("Jobs")==0){
                  out.print("<td>");
                  for(int e=0;e<listaRes.get(a).nameJobs.size();e++){
                    out.print(listaRes.get(a).nameJobs.get(e)+"</br>");
                  }
                  out.print("</td>");
                }
            }
           out.println("<td><button title=\"");
            
          for(String type: listaRes.get(0).type){
            out.print("Class="+type+" ");
          }
           out.print(listaRes.get(a).description+"\"type=\"button\" class=\"btn btn-default btn-lg\">INFO</button></td>");
            out.println("</tr>");
        }
%>

   </tbody> 
  </table>
<%
  }}
%>
</body>
</html>